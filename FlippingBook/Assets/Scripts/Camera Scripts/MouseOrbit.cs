using UnityEngine;

[AddComponentMenu("Camera-Control/Mouse Orbit")]
public class MouseOrbit : MonoBehaviour
{

	public Transform target;
	
	public float xSpeed = 250.0f;
	public float ySpeed = 0.0f;//120.0f;
	
	public float yMinLimit = -20.0f;
	public float yMaxLimit = 80.0f;
	
	private float x = 0.0f;
	private float y = 0.0f;
	
	private float distance = 10.0f;
	
	void Start () 
	{
	    Vector3 angles = transform.eulerAngles;
	    x = angles.y;
	    y = angles.x;
		
		if (target)
			distance = (target.position - transform.position).magnitude;
		
	
		// Make the rigid body not change rotation
	   	if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;
	}
	
	void LateUpdate () 
	{
	    if (target) 
		{
			// Gain the mouse movement
	        x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
	        y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
	 		
	 		y = ClampAngle(y, yMinLimit, yMaxLimit);
	 		       
	        Quaternion rotation;
			rotation = Quaternion.Euler(y, x, 0.0f);
	        Vector3 position = rotation * (new Vector3(0.0f, 0.0f, -distance)) + target.position;
	        
//	        transform.rotation = rotation;
//	        transform.position = position;
			
			transform.RotateAround(target.position, GetComponent<Camera>().cameraToWorldMatrix * Vector3.up, x);
	    }
	}
	
	static float ClampAngle (float angle, float min, float max) 
	{
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}

}