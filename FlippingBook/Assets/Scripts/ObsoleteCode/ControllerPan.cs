﻿using UnityEngine;
using System.Collections;

namespace ObsoleteCode
{
public class ControllerPan : MonoBehaviour
{
	// ?
	private static ControllerPan instance = null;
	public static ControllerPan Instance { get{ return instance; } }
	
	public SelectScrollItemDelegate selectScrollItemEvent;
	
	// target pos ?
	public Transform[] targetPosArray;
	
	public int curTargetIndex = 0;
	
	public float bookWidthOffset = 3;
	
	public Vector3 CameraRelativeUp
	{
		get
		{
			return GetComponent<Camera>().cameraToWorldMatrix * Vector3.up;
		}
	}
	
	// calculate the pos difference
	private Vector2 lastTouchPos = Vector2.zero;
	private Vector2 deltaTouchPos = Vector2.zero;
	private Vector2 curTouchPos = Vector2.zero;
	
	// temperary variables for pos calculation
	private Vector3 fromVec = Vector3.zero;
	private Vector3 toVec = Vector3.zero;
	
	
	private Transform transSphere ;
	private float magHit = 0.0f;
	
	
	// Set the boucing action enabled
	private bool bNeedAutoMove = false;
	private float actionPercent = 0.0f;
	bool clockwise = true;
	
	// record if the button or touch release in the frame
	private bool bIsUp = false;
	
	void OnDisable()
	{
//		selectScrollItemEvent = null;
	}
	
	
	void Awake()
	{
		instance = this;
		
		
		if(targetPosArray != null && targetPosArray.Length > 0)
		{
			// make camera focus on the first book
			if(curTargetIndex < targetPosArray.Length)
				transform.position = new Vector3(targetPosArray[curTargetIndex].position.x, transform.position.y, transform.position.z);
		}
		
//		float tempRange = Screen.width * 0.05f;
		transSphere = GameObject.FindGameObjectWithTag("Table").transform;
//		magHit = (transSphere.position - transform.position).magnitude;
		
		SmoothLookAt(transSphere);
	}
	
	
	
	void Update()
	{
		
		// Press down
		if(GameInputHelper.IsTouchDown(0))
		{
			// break when pressed and drag
//			if (GameInputHelper.IsKeepingTouch(0) == false)
			{
				lastTouchPos = GameInputHelper.GetScreenPosition(0);
				deltaTouchPos = lastTouchPos;
				curTouchPos = deltaTouchPos;
			}
		}
		// keep Press and Drag
		else if(GameInputHelper.IsKeepingTouch(0))
		{
			curTouchPos = GameInputHelper.GetScreenPosition(0);
			
			if(curTouchPos.x > 0 && curTouchPos.x < Screen.width)
			{
				// calcute the pos, need to be around a sphere
				// also need force feedback
				
				// translate accroding to a plain
//				transform.position += Vector3.right * (deltaTouchPos.x - curTouchPos.x) * 0.05f;
				// translate according to a sphere
				actionPercent = 1.0f;
				RotateAndTranslateTo(null, (deltaTouchPos.x < curTouchPos.x));
			}
			deltaTouchPos = curTouchPos;
		}
		
		// distinguish Click from DragEnd
		bIsUp = GameInputHelper.IsTouchUp(0);
			
	}
	
	
	void FixedUpdate()
	{
		if(bIsUp)
		{
			// the hitting collider
			RaycastHit hit;
			
			// calculate if drag
			float fDragDistance = (curTouchPos - lastTouchPos).magnitude;
			// if drag end
			if (fDragDistance > 0.25f)
			{
//				if (fDragDistance > (Screen.width * 0.25f))
//				{
//					if (curTouchPos.x < lastTouchPos.x)
//					{
//						if(targetPosArray != null && targetPosArray.Length > 0)
//							if(curTargetIndex < targetPosArray.Length - 1)
//								curTargetIndex++;
//					}
//					else
//					{
//						if(curTargetIndex > 0)
//							curTargetIndex--;
//					}
//				}
//				// bounce back to the original book
//				else
//				{
//				}
				// Camera.main is the old code, for the first enabled camera tagged as "MainCamera"
				Ray ray = GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.0f));
				bNeedAutoMove = Physics.Raycast(ray, out hit);//, magHit, LayerMask.NameToLayer("Book"));
				if (bNeedAutoMove == true)
				{
					curTargetIndex = hit.collider.gameObject.GetComponent<Book>().bookIndex;
					clockwise = (GetComponent<Camera>().WorldToScreenPoint(targetPosArray[curTargetIndex].position).x > curTouchPos.x);
//					curTargetIndex = hitIndex;
				}
				else
				{
					if (lastTouchPos.x != curTouchPos.x)
					{
						if (lastTouchPos.x > curTouchPos.x)
						{
							if (curTargetIndex != targetPosArray.Length - 1)
							{
								Debug.Log(Vector3.Angle(targetPosArray[curTargetIndex].position - transSphere.position, transform.position - transSphere.position));
								if ((Vector3.Angle(targetPosArray[curTargetIndex].position - transSphere.transform.position, targetPosArray[curTargetIndex + 1].position - transSphere.position) * 0.5) <
									Vector3.Angle(targetPosArray[curTargetIndex].position - transSphere.position, transform.position - transSphere.position))
								{
									curTargetIndex++;
									clockwise = false;
								}
								else
									clockwise = true;
							}
						}
						else
						{
							if (curTargetIndex != 0)
							{
								if ((Vector3.Angle(targetPosArray[curTargetIndex].position - transSphere.transform.position, targetPosArray[curTargetIndex - 1].position - transSphere.transform.position) * 0.5) <
									Vector3.Angle(targetPosArray[curTargetIndex].position - transSphere.position, transform.position - transSphere.position))
								{
									curTargetIndex--;
									clockwise = true;
								}
								else
									clockwise = false;
							}
						}
					}
					// situation that aint move on x-axis
					else
					{
						// do nothing, bounce back to current index
					}
				}
					
				bNeedAutoMove = true;
				actionPercent = 0.0f;
			}
			// if not drag
			else
			{
				if(selectScrollItemEvent != null)
				{
					Ray ray = GetComponent<Camera>().ScreenPointToRay(new Vector3(curTouchPos.x, curTouchPos.y, GetComponent<Camera>().nearClipPlane));
					bNeedAutoMove = Physics.Raycast(ray, out hit);//, magHit, LayerMask.NameToLayer("Book"));
					
					if (bNeedAutoMove == true)
					{
						Debug.Log("點書");
						
						actionPercent = 0.0f;
						curTargetIndex = hit.collider.gameObject.GetComponent<Book>().bookIndex;
						clockwise = (GetComponent<Camera>().WorldToScreenPoint(targetPosArray[curTargetIndex].position).x > curTouchPos.x);
						
						selectScrollItemEvent(curTargetIndex);
					}
				}
			}
			
		}
		
		if(bNeedAutoMove)
		{
			if(actionPercent < 1.0f)
			{
				actionPercent += Time.deltaTime * 2.5f;
				
//				SmoothMoveTo(targetPosArray[curTargetIndex]);
				RotateAndTranslateTo(targetPosArray[curTargetIndex], clockwise);
			}
			else
			{
				bNeedAutoMove = false;
//				actionPercent = 1.0f;
			}
		}
		
//		bIsUp = false;
		
	}
	
	void RotateAndTranslateTo(Transform target, bool clockwise = true)
	{
		float fAngle = 0.0f;
		if (target)
		{
			fAngle = Vector3.Angle(target.position - transSphere.position, 
				transform.position - transSphere.position);
		}
		else
		{
			if (Vector2.Distance(deltaTouchPos, curTouchPos) == 0.0f )
				return;
			
			Vector3 deltaPos = new Vector3(deltaTouchPos.x, deltaTouchPos.y, GetComponent<Camera>().nearClipPlane);
			Vector3 dragPos = new Vector3(curTouchPos.x, curTouchPos.y, GetComponent<Camera>().nearClipPlane);
			fAngle = Vector3.Angle(deltaPos - transSphere.position, dragPos - transSphere.position);
		}
			
		fAngle *= clockwise ? 1.0f : -1.0f;
		transform.RotateAround(transSphere.position, CameraRelativeUp, actionPercent * fAngle);
	}
	
	void SmoothMoveTo(Transform target)
	{
//		if (fromVec.Equals(Vector3.zero) && toVec.Equals(Vector3.zero))
		if (target)
		{
			fromVec = transform.position;
			toVec = new Vector3(target.position.x, fromVec.y, fromVec.z);
			
		
			// cannot use Slerp for the strange move
			transform.position = Vector3.Lerp(fromVec, toVec, actionPercent);
		}
	}
	
	void SmoothLookAt(Transform target)
	{
		if (target) 
		{
			// Look at and dampen the rotation smooth way
//			Quaternion rotation = Quaternion.LookRotation(target.position - transform.position, CameraRelativeUp);
//			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, actionPercent);
			
			
			transform.LookAt(target, CameraRelativeUp);
		}
	}
	
	void OnGUI()
	{
		if(GUILayout.Button("back"))
		{
			// all close
			selectScrollItemEvent(-1);
		}
	}
	
	
}
}