﻿////#define STEREO3D
//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//
//#pragma warning disable 0219 // variable assigned but not used.
//
//public class ScrollController : MonoBehaviour
//{
//    #region DELEGATE_TYPE
//    // for book animation callback
//    public SelectScrollItemDelegate selectScrollItemEvent;
//    #endregion
//
//    #region PRIVATE_USE
//    private static ScrollController _instance = null;
//    public static ScrollController Instance
//    {
//        get
//        {
//            return _instance;
//        }
//        set
//        {
//            _instance = value;
//        }
//    }
//
//    // target pos ?
//    private Transform curTargetTrans = null;
//    private Transform[] targetPosArray = null;
//    public int BookCount
//    {
//        get
//        {
//            return (targetPosArray == null ? 0 : targetPosArray.Length);
//        }
//    }
//    private Dictionary<int, Texture2D> listTexture = new Dictionary<int, Texture2D>();
//    public int TextureListCount
//    {
//        get
//        {
//            return listTexture.Count;
//        }
//    }
//
//    private float magHit = 0.0f;
//    public float CamToSphereDis
//    {
//        get
//        {
//            return magHit;
//        }
//    }
//    public Vector3 spotToLook = Vector3.zero;
//    public Transform transSphere = null;
//
//    // calculate the pos difference
//    private Vector2 lastTouchPos = Vector2.zero;
//    private Vector2 deltaTouchPos = Vector2.zero;
//    private Vector2 curTouchPos = Vector2.zero;
//
//    // Set the boucing action enabled
//    private bool bNeedAutoMove = false;
//    private float actionPercent = 1.0f;
//
//    private bool bConstraintTouch = false;
//    private GameObject worldObj = null;
//    // record the rotating direction
//    private int clockwise = 0;
//    private float fAmount = 0.0f;
//    // record if the button or touch release in the frame
//    private bool bIsUp = false;
//    private bool bIsTouchDown = false;
//    private bool bIsPressDown = false;
//
//    #endregion
//
//    #region UTILITY_PROPERTY
//    public Vector3 CameraCenterPoint
//    {
//        get
//        {
//            return camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, camera.nearClipPlane));
//        }
//    }
//    public Vector3 CameraRelativeUp
//    {
//        get
//        {
//            return camera.cameraToWorldMatrix * Vector3.up;
//        }
//    }
//    // just testing
//    public Vector3 CameraRelativeLook
//    {
//        get
//        {
//            return camera.cameraToWorldMatrix * Vector3.back;
//        }
//    }
//    #endregion
//
//    #region BOOK_OPERATION
//    // add the transform to array
//    public void AddGameObject(Object obj)
//    {
//        if (targetPosArray == null)
//            targetPosArray = System.Array.CreateInstance(typeof(Transform), 0) as Transform[];
//
//        int iIndex = (obj as Transform).GetComponent<Book>().bookIndex;
//        //        int iIndex = targetPosArray.Length;
//
//        // set objects more than texture invisible
//        if (iIndex >= listTexture.Count)
//        {
//            (obj as Transform).collider.isTrigger = false;
//            (obj as Transform).gameObject.SetActive(false);
//            return;
//        }
//        else
//        {
//            (obj as Transform).GetComponent<Book>().bookIndex = iIndex;
//            //(obj as Transform).gameObject.layer = LayerMask.NameToLayer(BOOK_LAYER);
//        }
//
//        if (iIndex >= targetPosArray.Length)
//            System.Array.Resize<Transform>(ref targetPosArray, iIndex + 1);
//        targetPosArray.SetValue(obj, iIndex);
//
//        SortGameObject();
//    }
//
//    void SortGameObject()
//    {
//        if (targetPosArray.Length == 0)
//            return;
//
//        // place each transform to equal angle place
//        float fAngle = 360.0f / targetPosArray.Length;
//        _invisibleCount = BookCount - (BookCount == TextureListCount ? BookCount - 1 :
//            (fAngle > 45.0f ? BookCount - 1 : BookCount - 3));
//
//        for (int iIndex = 0; iIndex < targetPosArray.Length; ++iIndex)
//        {
//            if (targetPosArray[0] == null)
//                return;
//
//            if (iIndex == 0)
//            {
//                // make each book all around sphere with same distance.
//                float magCameraDistance = magHit * 0.6f;
//                Vector3 vecFaceDirection = transSphere.position + (targetPosArray[iIndex].position - transSphere.position).normalized * magCameraDistance;
//                targetPosArray[iIndex].position = vecFaceDirection;
//            }
//            else
//            {
//
//                if ((targetPosArray[iIndex] != null) && (transSphere != null))
//                {
//                    targetPosArray[iIndex].position = targetPosArray[0].position;
//                    // make book lean forward
//                    targetPosArray[iIndex].LookAt(spotToLook, transSphere.up);
//                }
//            }
//
//            targetPosArray[0].RotateAround(transSphere.position, transSphere.up, -fAngle);
//
//        // make camera focus on the first book
//            if ((targetPosArray[iIndex] != null) && (targetPosArray[iIndex].GetComponent<Book>().bookIndex == Book.CurrFrontIndex))
//        	{
//                curTargetTrans = targetPosArray[iIndex];
//                clockwise = (camera.WorldToViewportPoint(curTargetTrans.position).x == 0.5f) ? 0 :
//                    (camera.WorldToViewportPoint(curTargetTrans.position).x < 0.5f) ? 1 : -1;
//                
//            	bNeedAutoMove = true;
//            	actionPercent = 0.0f;
//        	}
//    	}
//	}
//
//    void LoadTexture()
//    {
//        int iCount = 0;
//        //        string[] files = Directory.GetFiles(( Application.dataPath + "/Resources/Texture/").Replace("file://", ""), "*.*", SearchOption.TopDirectoryOnly);
//        //        foreach(string f in files)
//        //        {
//        //            if (f.Contains("meta") || f.Contains("map"))// || !f.Contains("jpg"))
//        //                continue;
//        //            Texture2D texture = null;
//        //            // here need to change as assetbundle version
//        //            if (f.Contains("tga"))
//        //            {
//        //                texture = LoadTGA(f);
//        //            }
//        //            else if (f.Contains("jpg"))
//        //            {
//        //                WWW cache = new WWW("file://" + f);
//        //                while (!cache.isDone)
//        //                    texture = cache.texture;
//        //                cache.Dispose();
//        //            }
//        string str = "Book/";//+ Path.GetFileNameWithoutExtension(f);
//        Object[] objArray = Resources.LoadAll(str, typeof(Texture2D));
//        foreach (Object obj in objArray)
//        {
//            Texture2D texture = obj as Texture2D;
//
//            // check done
//            if (texture != null)
//            {
//                if (External.CheckZoneTable(iCount))
//                    break;
//                else
//                listTexture.Add(iCount, texture);
//
//                iCount++;
//            }
//
//        }
//
//        System.Array.Clear(objArray, 0, objArray.Length);
//
//    }
//
//    public Texture2D TextureList(int iIndex)
//    {
//        Texture2D textu = default(Texture2D);
//        if (listTexture.ContainsKey(iIndex))
//        {
//            textu = listTexture[iIndex];
//        }
//
//        return textu;
//    }
//
//    public void DeSelect()
//    {
//        if (selectScrollItemEvent != null)
//            selectScrollItemEvent(TextureListCount * -1);
//    }
//
//    public void Select(int iIndex)
//    {
//        if (selectScrollItemEvent != null)
//            selectScrollItemEvent(iIndex);
//    }
//
//    public bool RotateTo(int iIndex)
//    {
//        if ((iIndex >= 0 && iIndex < TextureListCount) == false)
//            return false;
//
//        // modified by jackyli 2014/04/01 for repositioning back to zero
//        if (actionPercent < 1.0f && ((clockwise == 1) != (iIndex < Book.CurrFrontIndex)))
//            return true;
//
//        if (iIndex == Book.CurrFrontIndex)
//        {
//            Select(iIndex);
//            return false;
//        }
//
//
//        bool bFound = false;
//        int iNowObjectIndex = 0;
//        for (int i = 0; i < targetPosArray.Length; ++i)
//        {
//            if (curTargetTrans.Equals(targetPosArray[i]))
//                iNowObjectIndex = i;
//            if (targetPosArray[i].GetComponent<Book>().bookIndex == iIndex)
//            {
//                curTargetTrans = targetPosArray[i];
//                clockwise = iIndex < Book.CurrFrontIndex ? 1 : -1;
//
//                bNeedAutoMove = true;
//                actionPercent = 0.0f;
//
//                bFound = true;
//                break;
//            }
//        }
//
//        // can't find match in present objects
//        if (bFound == false)
//        {
//            int iIndexToGo = iIndex - Book.CurrFrontIndex;
//            iNowObjectIndex += (Mathf.Abs(iIndexToGo) < targetPosArray.Length) ? iIndexToGo : (iIndexToGo < 0) ? 1 : -1;
//            if ((iNowObjectIndex >= 0 && iNowObjectIndex < targetPosArray.Length) == false)
//                iNowObjectIndex += (iNowObjectIndex > 0) ? -targetPosArray.Length : targetPosArray.Length;
//            {
//                curTargetTrans = targetPosArray[iNowObjectIndex];
//
//                clockwise = iIndexToGo < 0 ? 1 : -1;
//                bNeedAutoMove = true;
//                actionPercent = 0.0f;
//            }
//        }
//
//        return true;
//    }
//
//    private int _invisibleCount = 0;
//    public int InvisibleCount
//    {
//        get
//        {
//            return _invisibleCount;
//        }
//    }
//
//    #endregion
//
//    #region MONOBEHAVIOUR
//    void Awake()
//    {
//        if (_instance == null)
//            _instance = this;
//
//        External.SetStereoCamera(this);
//        // suggested by clerk304
//        if (transSphere == null)
//            transSphere = GameObject.Find("Sphere").transform;
//
//        magHit = Vector3.Distance(transSphere.position, transform.position);
//        spotToLook = transSphere.position + Vector3.down * 5.0f;
//
//        // suggested by clerk304
//        if (transform.Find("Meters") != null)
//            transform.Find("Meters").animation.wrapMode = WrapMode.Loop;
//
//        if (TextureListCount == 0)
//            LoadTexture();
//    }
//
//
//    void Start()
//    {
//        SmoothLookAt(transSphere);
//    }
//
//    void OnDestroy()
//    {
//        System.Array.Clear(targetPosArray, 0, targetPosArray.Length);
//
//        Dictionary<int, Texture2D>.Enumerator textureEn = listTexture.GetEnumerator();
//        while(textureEn.MoveNext())
//            Resources.UnloadAsset(textureEn.Current.Value);
//        listTexture.Clear();
//
//        // suggested by clerk304
//        System.Delegate[] delegList = selectScrollItemEvent.GetInvocationList();
//        for (int i = 0; i < delegList.Length; ++i)
//            selectScrollItemEvent -= (SelectScrollItemDelegate)delegList[i];
//        selectScrollItemEvent = null;
//
//        listTexture.Clear();
//        System.GC.Collect();
//    }
//
//    void Update()
//    {
//        if (GameInputHelper.GetTouchCount() > 1)
//            return;
//
//        if (bConstraintTouch)
//            return;
//
//        bIsUp = GameInputHelper.IsTouchUp(0);
//        bIsTouchDown = GameInputHelper.IsTouchDown(0);
//        //bIsTouchDown = GameInputHelper.IsTouchDownTweak(0) && (GameInputHelper.GetTapCount() == 1);
//        bIsPressDown = GameInputHelper.IsKeepingTouch(0)  && (GameInputHelper.GetTapCount() == 1);
//
//        // Press down
//        if (bIsTouchDown)
//        {
//            if (GameInputHelper.GetScreenPosition(0).x > 0 && GameInputHelper.GetScreenPosition(0).x < Screen.width)
//            {
//                // block the situation of opened book
//                if (CheckAnim() == true && (GameInputHelper.GetTapCount() <= 1))
//                {
//                    actionPercent = 1.0f;
//    				RotateAndTranslateTo(curTargetTrans, clockwise);
//                }
//
//                lastTouchPos = GameInputHelper.GetScreenPosition(0);
//                deltaTouchPos = lastTouchPos;
//                curTouchPos = deltaTouchPos;
//
//                //transSphere.parent.rigidbody.angularVelocity = Vector3.zero;
//            }
//            //Debug.Log(string.Format("Update:TouchDown{0},{1},{2}", lastTouchPos.ToString(), curTouchPos.ToString(), GameInputHelper.GetTapCount().ToString()));
//        }
//        // keep Press and Drag
//        else if (bIsPressDown)
//        {
//
//            if (CheckAnim() == true)
//            {
////                actionPercent = 1.0f;
////                RotateAndTranslateTo(curTargetTrans, camera.WorldToViewportPoint(curTargetTrans.position).x == 0.5f ? 0 : camera.WorldToViewportPoint(curTargetTrans.position).x < 0.5f ? 1 : -1);
//            }
//            else
//            {
//
//            	curTouchPos = GameInputHelper.GetScreenPosition(0);
////                fAmount = 0.0f;
//            	if (curTouchPos.x > 0 && curTouchPos.x < Screen.width)
////                if ((curTouchPos.x > 0 && curTouchPos.x < Screen.width) == false)
////                    curTouchPos = deltaTouchPos;
//                {
//	                // calcute the pos, need to be around a sphere
//	                // also need force feedback
//
//	                // translate according to a sphere
//	                actionPercent = 1.0f;
//		            clockwise = (deltaTouchPos.x == curTouchPos.x) ? 0 : (deltaTouchPos.x < curTouchPos.x) ? 1 : -1;
//		            // block the operation that exceeds the limit.
////		            if (Book.CheckSelectedIndex(clockwise) == false)
////		            {
////		                fAmount = 0.0f;
////		                bIsUp = true;
////						RotateAndTranslateTo(curTargetTrans, clockwise);
////		            }
////		            else
//		            {
//	                	RotateAndTranslateTo(null, clockwise);
//	            		fAmount = (curTouchPos.x - deltaTouchPos.x) / 3.0f;
//	            	}
//
////                    clockwise = (lastTouchPos.x == curTouchPos.x) ? 0 : (lastTouchPos.x < curTouchPos.x) ? 1 : -1;
////                    fAmount = ((curTouchPos.x - lastTouchPos.x) * 0.125f) / camera.nearClipPlane;
//                }
//
//            	deltaTouchPos = curTouchPos;
//        	}
//            //Debug.Log(string.Format("Update:PressDown{0},{1},{2}", lastTouchPos.ToString(), curTouchPos.ToString(), GameInputHelper.GetTapCount().ToString()));
//        }
//
//    }
//
//    //const string BOOK_LAYER = "3dGUI";
//    //const string GUI_LAYER = "2dGUI";
//
//    void LateUpdate()
//    {
//        bConstraintTouch = External.Check2D() || CheckAnim(false);
//        if (GameInputHelper.GetTouchCount() > 1)
//            return;
//
//        // only trigger when torgue is end.
//        //if (transSphere.parent.rigidbody.angularVelocity.magnitude != 0)
//        //  return;
//        if ((fAmount != 0.0f) && (GameInputHelper.IsKeepingTouch(0) == false))
////        if (bIsTouchUp == false && bIsPressDown == true)
//        {
//            //transSphere.parent.rigidbody.AddTorque(transSphere.parent.up * fAmount, ForceMode.Impulse);
//            curTouchPos.x += fAmount;
//            float fTemp = fAmount + Time.deltaTime * 10.0f * -Mathf.Sign(fAmount);
//            //deltaTouchPos.x -= fAmount;
//
//            //Debug.Log(fTemp);
//
//            actionPercent = 1.0f;
//            RotateAndTranslateTo(null, (deltaTouchPos.x == curTouchPos.x) ? 0 : (deltaTouchPos.x < curTouchPos.x) ? 1 : -1);
//            //RotateAndTranslateTo(null, clockwise);
//
//            deltaTouchPos = curTouchPos;
//
//            if ((Book.CheckSelectedIndex(clockwise) == false) 
//                || (Mathf.Sign(fTemp) != Mathf.Sign(fAmount)) )
//            {
//                fAmount = 0.0f;
//                bIsUp = true;
//            }
//            else
//            {
//                fAmount = fTemp;
//                return;
//            }
//
////            if (Book.CheckSelectedIndex(clockwise))
////            {
////                deltaTouchPos.x -= fAmount;
////                //int iTempclockwise = (deltaTouchPos.x == curTouchPos.x) ? 0 : (deltaTouchPos.x < curTouchPos.x) ? 1 : -1;
////                
////                actionPercent = 1.0f;
////                RotateAndTranslateTo(null, clockwise);
////                
////                deltaTouchPos = curTouchPos;
////                return;
////            }
////            else
////            {
////                int iIndexNow = System.Array.FindIndex(targetPosArray, tran => tran.GetComponent<Book>().bookIndex == Book.CurrFrontIndex);
////                curTargetTrans = targetPosArray[iIndexNow];
////                actionPercent = 1.0f;
////                RotateAndTranslateTo(curTargetTrans, clockwise);
////            }
//        }
//
//        if (bIsUp)
//        {
////                int iIndexNow = System.Array.FindIndex(targetPosArray, tran => tran.GetComponent<Book>().bookIndex == Book.CurrFrontIndex);
////                int iNext = iIndexNow + (-clockwise);
////                if (iNext < 0) iNext = BookCount - 1;
////                if (iNext > BookCount - 1) iNext = 0;
//            // the hitting collider
//            RaycastHit hit;
//            Ray ray;
//            // calculate if drag
//            float fDragDistance = (curTouchPos - lastTouchPos).magnitude;
//            // if drag end
//            if (fDragDistance > 10.0f)
//            {
//                // Camera.main is the old code, for the first enabled camera tagged as "MainCamera"
//                ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, camera.nearClipPlane));
//                bNeedAutoMove = Physics.Raycast(ray, out hit, magHit);//, 1 << LayerMask.NameToLayer("Book"));
//                if (bNeedAutoMove == true)
//                {
//                    if (hit.collider.isTrigger == true)
//                        curTargetTrans = hit.collider.transform;
//                    clockwise = (camera.WorldToViewportPoint(curTargetTrans.position).x == 0.5f) ? 0 :
//                        (camera.WorldToViewportPoint(curTargetTrans.position).x < 0.5f) ? 1 : -1;
//                }
//                else
//                {
//                    CheckMinAngle(out curTargetTrans, out clockwise);
//                }
//
//                bNeedAutoMove = true;
//                actionPercent = 0.0f;
//                if (Book.CheckSelectedIndex(clockwise) == false)
//                    actionPercent = 1.0f;
//
////                bNeedAutoMove = true;
////                actionPercent = 0.0f;
////                if (Book.CheckSelectedIndex(clockwise)
////                  && (targetPosArray[iNext].collider.isTrigger == true))
////                    curTargetTrans = targetPosArray[iNext];
////                else
////                {
////                    curTargetTrans = targetPosArray[iIndexNow];
////                    clockwise = camera.WorldToViewportPoint(curTargetTrans.position).x == 0.5f ? 0 : 
////                        camera.WorldToViewportPoint(curTargetTrans.position).x < 0.5f ? 1 : -1;
////                    actionPercent = 1.0f;
////                }
//            }
//            // if not drag
//            else
//            {
//                ray = camera.ScreenPointToRay(new Vector3(curTouchPos.x, curTouchPos.y, camera.nearClipPlane));
//                bNeedAutoMove = Physics.Raycast(ray, out hit, magHit);//, 1 << LayerMask.NameToLayer("Book"));
//
//                if (bNeedAutoMove == true && CheckAnim() == false
//                    && (hit.collider.isTrigger == true))
//                {
//                    //Debug.Log("點書");
//
//                    actionPercent = 0.0f;
//                    Transform hitTrans = hit.collider.transform;
////                    clockwise = camera.ScreenToViewportPoint(curTouchPos).x == 0.5f ? 0 :
////                        camera.ScreenToViewportPoint(curTouchPos).x < 0.5f ? 1 : -1;
//                    clockwise = camera.WorldToViewportPoint(hitTrans.position).x == 0.5f ? 0 :
//                        camera.WorldToViewportPoint(hitTrans.position).x < 0.5f ? 1 : -1;
//
//                    if (hitTrans.Equals(curTargetTrans))
//                    //if (Vector3.Angle(transform.forward, transSphere.position - hitTrans.position) < 5.0f)
//                    {
//                        if (selectScrollItemEvent != null)
//                            selectScrollItemEvent(curTargetTrans.GetComponent<Book>().bookIndex);
//
//                        //bNeedAutoMove = false;
//                        actionPercent = 1.0f;
////                        bNeedAutoMove = clockwise != 0 ? true : false;
////                        actionPercent = bNeedAutoMove ? 0.0f : 1.0f;
//                    }
//
//                    curTargetTrans = hitTrans;
//                }
//                else
//                {
//                    CheckMinAngle(out curTargetTrans, out clockwise);
//                    bNeedAutoMove = true;
//                    actionPercent = 0.0f;
//                    if (Book.CheckSelectedIndex(clockwise) == false)
//                        actionPercent = 1.0f;
//
////                    curTargetTrans = targetPosArray[iIndexNow];
////                    clockwise = camera.WorldToViewportPoint(curTargetTrans.position).x == 0.5f ? 0 :
////                        camera.WorldToViewportPoint(curTargetTrans.position).x < 0.5f ? 1 : -1;
////                    bNeedAutoMove = clockwise != 0 ? true : false;
////                    actionPercent = bNeedAutoMove ? 0.0f : 1.0f;
//                }
//
//            }
//
//        }
//
//        if (bNeedAutoMove)
//        {
//            if (actionPercent < 1.0f)
//                actionPercent += Time.deltaTime * 0.55f; //1.0f
//
//            RotateAndTranslateTo(curTargetTrans, clockwise);
//        }
//        else if (bConstraintTouch)
//        {
//            if (actionPercent < 2.0f)
//                actionPercent += Time.deltaTime;
//            else
//            {
//                bConstraintTouch = false;
//                actionPercent = 1.0f;
//            }
//
//        }
//
//        bIsUp = false;
//
//    }
//    void OnGUI()
//    {
//        if (External.GetMainProcMgr() == null)
//            if (GUILayout.Button("back"))
//            {
//                // all close
//                DeSelect();
//            }
//    }
//    #endregion
//
//
//    #region CAMERA_ACTION
//    void RotateAndTranslateTo(Transform target, int iclockwise = 0)
//    {
//        float fAngle = 0.0f;
//        if (target)
//        {
//            //adjust rolling speed
//            fAngle = Vector3.Angle(target.position - transSphere.position, transform.position - transSphere.position);
//            if (fAngle >= 0.1f)
//                fAngle *= actionPercent;
//            else
//            {
//                bNeedAutoMove = false;
//                actionPercent = 1.0f;
//                clockwise = 0;
//            }
//        }
//        else
//        {
//            Vector3 deltaPos = camera.ScreenToWorldPoint(new Vector3(deltaTouchPos.x, Screen.height * 0.5f, camera.nearClipPlane));
//            Vector3 dragPos = camera.ScreenToWorldPoint(new Vector3(curTouchPos.x, Screen.height * 0.5f, camera.nearClipPlane));
//            fAngle = Vector3.Angle(deltaPos - transSphere.position, dragPos - transSphere.position) * camera.nearClipPlane;
//            // drag speed up plus 50%
//            //fAngle *= 1.5f;
//        }
//
//
//        fAngle *= iclockwise;
//        // rotate camera itself
//        transform.RotateAround(transSphere.position, CameraRelativeUp, fAngle);
//        // rotate the books
//        //transSphere.parent.Rotate(transSphere.up, fAngle, Space.Self);
//    }
//
//    void SmoothMoveTo(Transform target)
//    {
//        //if (fromVec.Equals(Vector3.zero) && toVec.Equals(Vector3.zero))
//        if (target)
//        {
//            Vector3 fromVec = transform.position;
//            Vector3 toVec = new Vector3(target.position.x, fromVec.y, fromVec.z);
//
//
//            // cannot use Slerp for the strange move
//            transform.position = Vector3.Lerp(fromVec, toVec, actionPercent);
//        }
//    }
//
//    void SmoothLookAt(Transform target)
//    {
//        if (target)
//        {
//            // Look at and dampen the rotation smooth way
//            //Quaternion rotation = Quaternion.LookRotation(target.position - transform.position, CameraRelativeUp);
//            //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, actionPercent);
//
//            transform.LookAt(target, CameraRelativeUp);
//
//            External.SetPivot(target);
//        }
//    }
//
//    #endregion
//
//    #region CHECK_FUNCTION
//    float CheckMinAngle(out Transform tTrans, out int iclockwise)
//    {
//        Vector3 vecCam = transform.forward;
//        // make it the biggest so that there can only be the min
//        float fAngle = 180.0f;
////        int iStart = System.Array.FindIndex(targetPosArray, 
////            elem => elem.GetComponent<Book>().bookIndex == Book.CurrFrontIndex);
//
//        Transform tempTrans = null;
////		for (int i = iStart + (-clockwise) ;
////		     //i >= 0 && i < BookCount 
////		     ; i += clockwise)
//        for (int i = 0 ; i < BookCount ; ++i)
//        {
////            if (i < 0) i = BookCount - 1;
////            if (i > BookCount - 1) i = 0;
//
//			if (targetPosArray[i] == null)
//				continue;
//
//			// comment by jackyli 2013/12/16
//			// this vector has a little deviation about leaning forward.
//			// yet doesn't matter when calculate the min angle differences.
//            Vector3 vectemp = transSphere.position - targetPosArray[i].position;
//            float ftempAngle = Vector3.Angle(vecCam, vectemp);
//            if (ftempAngle < fAngle)
//            {
//				if (targetPosArray[i].collider.isTrigger == false)
//				{
//					// set the force acceleration to 0 ?
//					//tempTrans = curTargetTrans;
////					if (targetPosArray[i].GetComponent<Book>().bookIndex == Book.CurrFrontIndex)
////						break;
////					else
//						continue;
//				}
//				fAngle = ftempAngle;
//                tempTrans = targetPosArray[i];
//            }
//
////			if (i == iStart)
////				break;
//        }
//
//        tTrans = tempTrans;
//        iclockwise = (camera.WorldToViewportPoint(tTrans.position).x == 0.5f) ? 0 :
//            (camera.WorldToViewportPoint(tTrans.position).x < 0.5f) ? 1 : -1;
//
//        return fAngle;
//    }
//
//    public bool CheckAnim(bool bCheckFlip = true)
//    {
//        bool bIsAnim = false;
//        for (int i = 0; i < targetPosArray.Length; ++i)
//        {
//            // check if book opened as well
//            bIsAnim |= targetPosArray[i].GetComponent<Book>().CheckAnim;
//        }
//        if (bCheckFlip)
//		{
//        	bIsAnim |= (actionPercent < 1.0f);
//			if (worldObj != null)
//				bIsAnim |= (worldObj.activeSelf == false);
//		}
//        return bIsAnim;
//    }
//    #endregion
//
////    // download from internet quote from http://forum.unity3d.com/threads/172291-TGA-Loader-for-Unity3D
////    public Texture2D LoadTGA(string TGAFile)
////    {
////    
////        using (BinaryReader r = new BinaryReader(File.Open(TGAFile, FileMode.Open))) 
////        {
////    
////            byte IDLength = r.ReadByte();
////    
////            byte ColorMapType = r.ReadByte();
////            
////            byte ImageType = r.ReadByte();
////            
////            short CMapStart = r.ReadInt16();
////            
////            short CMapLength = r.ReadInt16();
////            
////            byte CMapDepth = r.ReadByte();
////            
////            short XOffset = r.ReadInt16();
////            
////            short YOffset = r.ReadInt16();
////            
////            short Width = r.ReadInt16();
////            
////            short Height = r.ReadInt16();
////            
////            byte PixelDepth = r.ReadByte();
////            
////            byte ImageDescriptor = r.ReadByte();
////            
////            if (ImageType == 0) 
////            {
////            
////                Debug.Log("Unsupported TGA file! No image data");
////            
////            }
////            else if (ImageType == 3 | ImageType == 11) 
////            {
////            
////                Debug.Log("Unsupported TGA file! Not truecolor");
////            
////            }
////            else if (ImageType == 9 | ImageType == 10) 
////            {
////            
////                Debug.Log("Unsupported TGA file! Colormapped");
////            }
////            
////            //     MsgBox("Dimensions are " & Width & "," & Height)
////            
////            Texture2D b = new Texture2D(Width, Height);
////            
////            Color32[] cl = System.Array.CreateInstance(typeof(Color32), b.width * b.height) as Color32[];
////            
////            for (int y = 0; y <= b.height - 1; y++) 
////            {
////                
////                for (int x = 0; x <= b.width - 1; x++) 
////                {
////            
////                    
////            
////                    if (PixelDepth == 32) 
////                    {
////            
////                     
////            
////                        byte red = r.ReadByte();
////                        
////                        byte green = r.ReadByte();
////                        
////                        byte blue = r.ReadByte();
////                        
////                        byte alpha = r.ReadByte();
////                        
//////                        alpha /=255;
//////                        
//////                        green /= 255;
//////                        
//////                        blue /= 255;
//////                        
//////                        red /= 255;
////                        
////                        cl[y * b.width + x] = new Color32(blue ,green ,red ,alpha );
////                        
////                    }
////                    else 
////                    {
////                        byte red = r.ReadByte();
////                
////                        byte green = r.ReadByte();
////                
////                        byte blue = r.ReadByte();
////                
//////                        green /= 255;
//////                
//////                        blue /= 255;
//////                
//////                        red /= 255;
////                
////                        cl[y * b.width + x] =  new Color32(blue ,green ,red ,1 );
////                
////                    }
////                    
////                }
////                
////                
////            }
////            b.SetPixels32(cl);
////
////            b.Apply();
////            return b;
////            
////        }
////        
////    }
//}
//
//
