﻿#define UNITY_ANDROID

using UnityEngine;
using System.Collections;

public static class GameInputHelper
{
//    private static int playerLayerIndex = 10;
//    public static int PlayerLayer { get { return 1 << playerLayerIndex; } }
//
//    private static int monsterLayerIndex = 11;
//    public static int LayerDefines.MonsterLayer { get { return 1 << monsterLayerIndex; } }
//
//    private static int groundLayerIndex = 12;
//    public static int GroundLayer { get { return 1 << groundLayerIndex; } }
//
//    private static int characterPlusGroundLayer = 1 << playerLayerIndex | 1 << monsterLayerIndex | 1 << groundLayerIndex;
//    public static int CharacterPlusGroundLayer { get { return characterPlusGroundLayer; } }


    public static int GetTouchCount()
    {
    #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
		    return Input.touchCount;
    #else
		    return 1;
    #endif
    }


    public static int GetTapCount()
    {
    #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
            if(Input.touchCount > 0)
                return Input.GetTouch(0).tapCount;
            else
                return 0;
    #else
            return 1;
    #endif
    }

    // the TouchPhase wouldn't quit Stationary status after NGUI click.
    // Which is triggering awkward behaviour when this scroll mechanism attaches on game GUI.
    public static bool IsTouchDownTweak(int num)
    {
        #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
        if(Input.touchCount > num && (Input.GetTouch(num).phase == TouchPhase.Began || Input.GetTouch(num).phase == TouchPhase.Stationary))
            return true;
        else
            return false;
        #else
        return Input.GetMouseButtonDown(0);
        #endif
    }

    public static bool IsTouchDown(int num)
    {
    #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
		    if(Input.touchCount > num && Input.GetTouch(num).phase == TouchPhase.Began)
			    return true;
            else
			    return false;
    #else
            return Input.GetMouseButtonDown(0);
    #endif
    }


    public static bool IsKeepingTouch(int num)
    {
    #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
		    if(Input.touchCount > num && (Input.GetTouch(num).phase == TouchPhase.Stationary || Input.GetTouch(num).phase == TouchPhase.Moved))
			    return true;
            else
			    return false;
    #else
        return Input.GetMouseButton(0);
    #endif
    }


    public static bool IsTouchUp(int num)
    {
    #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
		    if(Input.touchCount > num && Input.GetTouch(num).phase == TouchPhase.Ended)
			    return true;
            else
			    return false;
    #else
        return Input.GetMouseButtonUp(num);
    #endif
    }

	public static Vector3 GetLastInputPosition(int num)
	{
		return Input.GetTouch(num).deltaPosition;
	}

    public static Vector3 GetScreenPosition(int num)
    {
    #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
		return Input.GetTouch(num).position;
    #else
        return Input.mousePosition;
    #endif	
    }
	
	public static Vector3 GetAcceleration()
	{
    #if ((UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR)
		return Input.gyro.userAcceleration;
    #else
        return Vector3.zero;
    #endif	
	}
}
