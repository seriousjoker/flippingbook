﻿using UnityEngine;
using System.Collections;

// for book animation callback
public delegate void SelectScrollItemDelegate(int itemIndex);

public class Book : MonoBehaviour 
{
	
    private static int _currFrontIndex = 0;
    public static int CurrFrontIndex
    {
        get
        {
            return _currFrontIndex;
        }
    }
    public static bool CheckSelectedIndex(int clockwise = 0)
    {
        if (clockwise == 0)
            return (_currFrontIndex >= 0 && _currFrontIndex < ScrollController.Instance.TextureListCount );
        else
            return !(((_currFrontIndex <= 0 && clockwise == 1) 
                      || (_currFrontIndex >= ScrollController.Instance.TextureListCount - 1 && clockwise == -1)) );
    }
	public int _bookIndex = 0;
	public int bookIndex
    {
        get
        {
            return _bookIndex;
        }
        set
        {
            _bookIndex = value;
            ChangeFace(_bookIndex);
        }
    }
    private bool bCanChangeFace = false;
	
	private bool isOpen = false;
    private bool bMoveZ = false;
    Animation _bookAnim = null;
    public bool CheckAnim
    {
        get
        {
            return isOpen | (_bookAnim ? _bookAnim.isPlaying : false);
        }
    }
	
	private Vector3 fromPos = Vector3.zero;
	private Vector3 toPos = Vector3.zero;
	
	private float pagePercent = 1.0f;
    private float fAnimTime = 1.0f;
	private Vector3 bookOffset = Vector3.zero;
	
	
    
	void Start()
	{
		ScrollController.Instance.selectScrollItemEvent += BookAction;
		ScrollController.Instance.AddGameObject(this.transform);
			
	}

    void OnDestroy()
    {
        Book._currFrontIndex = 0;
    }

    bool bHasChecked = false;
    void CheckLast()
    {
        if (bHasChecked == false)
        {
            if (bookIndex >= ScrollController.Instance.BookCount - ScrollController.Instance.InvisibleCount)
                bookIndex += (-1 * ScrollController.Instance.BookCount);
            bHasChecked = true;
        }
    }
	
	void Update()
	{
        CheckLast();
        
		// action start when enabled
		if(pagePercent < fAnimTime)
		{
            pagePercent += Time.deltaTime;
            
            
            if (pagePercent > fAnimTime)
            {
                pagePercent = fAnimTime;
                bMoveZ = true;
            }

            transform.GetChild(0).localPosition = Vector3.Lerp(fromPos, toPos, pagePercent / fAnimTime);
			
		}
		else
		{
            // popup the 2D GUI menu
            if (bMoveZ)
            {
                _bookAnim[_bookAnim.clip.name].time = _bookAnim[_bookAnim.clip.name].speed < 0 ? 0.0f : _bookAnim.clip.length;
                fromPos = transform.GetChild(0).localPosition;
                toPos.z = (isOpen == false) ? 0.0f : -3.5f;
                if (Vector3.Distance(fromPos, toPos) > 0.05f)
                {
                    // modified by jackyli 2014/01/27 make the zoom in quicker
                    transform.GetChild(0).localPosition = Vector3.Lerp(fromPos, toPos, Time.deltaTime * 6);
                }
                else
                {

                    External.Action(isOpen, bookIndex);
                    bMoveZ = false;
                }
            }
		}
	}
	
	public void BookAction(int iIndex)
	{

		Transform tempTrans;
		
        if (_bookAnim && _bookAnim.isPlaying) return;
     
		// Opening action
		if(iIndex == this._bookIndex)
		{

			// set the open anim
			if (isOpen == false)
			{
                if (External.Select(bookIndex) == false)
                    return;
                
                {
                    tempTrans = transform.GetChild(0);
                    fromPos = tempTrans.localPosition;
                    toPos = new Vector3( fromPos.x + 2.8f, fromPos.y - 0.4f, fromPos.z);
//                        (tempTrans.localToWorldMatrix * (ScrollController.Instance.CenterPoint() - tempTrans.position))
                    
                    // set animation
                    tempTrans.GetComponent<Animation>().wrapMode = WrapMode.Once;
                    fAnimTime = tempTrans.GetComponent<Animation>().clip.length / 8.0f;
                    
                    if (_bookAnim == null)
                        _bookAnim = tempTrans.GetComponent<Animation>();
                    _bookAnim[_bookAnim.clip.name].speed = 8.0f;
                    _bookAnim[_bookAnim.clip.name].time = 0.0f;
                    _bookAnim.Play();
                }
				
                bookOffset = fromPos;
                pagePercent = 0.0f;

//				Debug.Log("Open Book" + this._bookIndex);
			}
			
			isOpen = true;
		}
		// Closing action
		else
		{
			// set the close anim
			if (isOpen == true)
			{
                {
                    // set close animation
                    tempTrans = transform.GetChild(0);
                    fromPos = tempTrans.localPosition;
                    toPos = bookOffset;
                    
                    // set animation in reverse
                    _bookAnim[_bookAnim.clip.name].speed = -8.0f;
                    _bookAnim[_bookAnim.clip.name].time = _bookAnim.clip.length;
                    _bookAnim.Play();
				}
                
                bookOffset = Vector3.zero;
                pagePercent = 0.0f;

//				Debug.Log("Close Book" + this._bookIndex);
			}
			
			isOpen = false;
		}
	}
	
    void FixedUpdate()
	{
        float fDistance = ScrollController.Instance.CamToSphereDis;
		Vector3 vecCam = ScrollController.Instance.CameraRelativeLook;
		Vector3 vecDis = transform.position - ScrollController.Instance.transform.position;
		float fAngle = Vector3.Angle(vecCam, vecDis);
		Vector3 vecNewFaceTo = Vector3.zero;
		bool bIsNeighbor = false;
		if ((fAngle > 30.0f))
		{
			if (Vector3.MoveTowards(ScrollController.Instance.transform.position, transform.position, fDistance).Equals(transform.position))
			{
				float fXOffset = 0.5f;
				
				// multiply so the point can be near
                vecNewFaceTo = transform.position - ScrollController.Instance.GetComponent<Camera>().ViewportToWorldPoint(
					new Vector3(fXOffset, 1.0f, ScrollController.Instance.GetComponent<Camera>().nearClipPlane)) * 5.0f;
				
				bIsNeighbor = true;
			}
		}
        else
        {
            // filter the two gameobjects on the line of camera looking at
            if (gameObject.activeSelf == true)
            {
                // change BookFace when on the back side of camera
                // calculate the opposite position of the camera against the sphere
                Vector3 vecBackCam = ScrollController.Instance.transform.position + ScrollController.Instance.CameraRelativeLook.normalized * fDistance * 2.0f;
                if (Vector3.MoveTowards(vecBackCam, transform.position, fDistance * 0.75f).Equals(transform.position))
                {
                    bCanChangeFace = true;
                }
                else if (bCanChangeFace == true)
                {
                    // the back side one
                    int iIndex = ScrollController.Instance.GetComponent<Camera>().WorldToViewportPoint(transform.position).x == 0.5f ? 0 :
                        ScrollController.Instance.GetComponent<Camera>().WorldToViewportPoint(transform.position).x < 0.5f ? -1 : 1;

                    if ((iIndex != 0) && CheckSelectedIndex(iIndex))
                    {
                        if ( ((iIndex > 0) && (_bookIndex < _currFrontIndex )) ||
                            ((iIndex < 0) && (_bookIndex > _currFrontIndex )) )
                        {
                            int iNext = _bookIndex + iIndex * ScrollController.Instance.BookCount;
                            // check the start before start, end after end
                            if ((iNext >= -1 * ScrollController.Instance.InvisibleCount) && 
                                (iNext < ScrollController.Instance.TextureListCount + ScrollController.Instance.InvisibleCount))
                                bookIndex = iNext;
                            
                        }
                    }
                    
                    bCanChangeFace = false;
                }
                else if (Vector3.MoveTowards(ScrollController.Instance.transform.position, transform.position, fDistance * 0.75f).Equals(transform.position))
                {
                    // the front side one
                    _currFrontIndex = _bookIndex;
                    External.ChangeChapter(_currFrontIndex);
                }
            }
        }
		
		if (bIsNeighbor == false)
            vecNewFaceTo = ScrollController.Instance.spotToLook -transform.position;
		
        transform.rotation = Quaternion.LookRotation(vecNewFaceTo);
	}
    
    
    private void ChangeFace(int iIndex)
    {
        // change face
        SkinnedMeshRenderer rend = transform.Find("Book/Book_A").GetComponent<Renderer>() as SkinnedMeshRenderer;
        if (rend != null)
        {
            Texture2D textu = ScrollController.Instance.GetTexture(iIndex);
            if ( textu != default(Texture2D))
            {
                rend.enabled = true;
                rend.materials[0].mainTexture = textu;
				
				//這邊看起來沒有改到texture的設定，所以應該不用call Apply() 
				//(為了call Apply()必須將texture格式設成Read/Write Enable，但這樣Unity會複製兩份貼圖到記憶體裡，所以我先註解掉)  凱鈞 2013/12/28
                //textu.Apply();
                GetComponent<Collider>().isTrigger = true;
            }
            else
            {
                rend.enabled = false;
                GetComponent<Collider>().isTrigger = false;
            }
        }
    }


// Don't DELETE this function
//    void OnDrawGizmosSelected()
//    {
//        Vector3 vecCam = ScrollController.Instance.CameraRelativeLook;
//        
//        vecCam = ScrollController.Instance.transform.position + vecCam.normalized * (ScrollController.Instance.transSphere.position - ScrollController.Instance.transform.position).magnitude * 2.0f ;
//        Gizmos.color = Color.white;
//        Gizmos.DrawLine(vecCam, Vector3.MoveTowards(vecCam, transform.position, (ScrollController.Instance.transSphere.position - ScrollController.Instance.transform.position).magnitude * 0.75f));
//        
//        Gizmos.color = Color.red;
//        Gizmos.DrawLine(ScrollController.Instance.transform.position, Vector3.MoveTowards(ScrollController.Instance.transform.position,
//            transform.position, (ScrollController.Instance.transSphere.position - ScrollController.Instance.transform.position).magnitude));
//    }
}
