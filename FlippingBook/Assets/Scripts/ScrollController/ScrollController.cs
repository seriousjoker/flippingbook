//#define STEREO3D
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

#pragma warning disable 0219 // variable assigned but not used.

public class ScrollController : MonoBehaviour
{
    // for book animation callback
    public SelectScrollItemDelegate selectScrollItemEvent = delegate{};
    #region SINGLETON
    private static ScrollController _instance = null;
    public static ScrollController Instance
    {
        get
        {
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }
    #endregion

    #region PRIVATE_USE
    // target pos ?
    private Transform curTargetTrans = null;
    private Transform[] targetPosArray = null;
    public int BookCount
    {
        get
        {
            return (targetPosArray == null ? 0 : targetPosArray.Length);
        }
    }
    private Dictionary<int, Texture2D> listTexture = new Dictionary<int, Texture2D>();
    public int TextureListCount
    {
        get
        {
            return listTexture.Count;
        }
    }

    private float magHit = 0.0f;
    public float CamToSphereDis
    {
        get
        {
            return magHit;
        }
    }
    public Vector3 spotToLook = Vector3.zero;
    public Transform transSphere = null;

    // calculate the pos difference
    private Vector2 lastTouchPos = Vector2.zero;
    private Vector2 deltaTouchPos = Vector2.zero;
    private Vector2 curTouchPos = Vector2.zero;

    // Set the boucing action enabled
    private bool bNeedAutoMove = false;
    private float actionPercent = 1.0f;

    private bool bConstraintTouch = false;
    private GameObject worldObj = null;
    // record the rotating direction
    private int clockwise = 0;
    private float fAmount = 0.0f;
    // record if the button or touch release in the frame
    private bool bIsTouchUp = false;
    private bool bIsTouchDown = false;
    private bool bIsPressDown = false;

    #endregion

    #region UTILITY_PROPERTY
    public Vector3 CameraCenterPoint
    {
        get
        {
            return GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, GetComponent<Camera>().nearClipPlane));
        }
    }
    public Vector3 CameraRelativeUp
    {
        get
        {
            return GetComponent<Camera>().cameraToWorldMatrix * Vector3.up;
        }
    }
    // just testing
    public Vector3 CameraRelativeLook
    {
        get
        {
            return GetComponent<Camera>().cameraToWorldMatrix * Vector3.back;
        }
    }
    #endregion

    #region BOOK_OPERATION
    // add the transform to array
    public void AddGameObject(Object obj)
    {
        if (targetPosArray == null)
            targetPosArray = System.Array.CreateInstance(typeof(Transform), 0) as Transform[];

        int iIndex = (obj as Transform).GetComponent<Book>().bookIndex;
        //        int iIndex = targetPosArray.Length;

        // set objects more than texture invisible
        if (iIndex >= listTexture.Count)
        {
            (obj as Transform).GetComponent<Collider>().isTrigger = false;
            (obj as Transform).gameObject.SetActive(false);
            return;
        }
        else
        {
            (obj as Transform).GetComponent<Book>().bookIndex = iIndex;
            (obj as Transform).gameObject.layer = LayerMask.NameToLayer(BOOK_LAYER);
        }

        if (iIndex >= targetPosArray.Length)
            System.Array.Resize<Transform>(ref targetPosArray, iIndex + 1);
        targetPosArray.SetValue(obj, iIndex);

        SortGameObject();
    }

    void SortGameObject()
    {
        if (targetPosArray.Length == 0)
            return;

        // place each transform to equal angle place
        float fAngle = 360.0f / targetPosArray.Length;
        _invisibleCount = BookCount - (BookCount == TextureListCount ? BookCount - 1 :
            (fAngle > 45.0f ? BookCount - 1 : BookCount - 3));

        for (int iIndex = 0; iIndex < targetPosArray.Length; ++iIndex)
        {
            if (targetPosArray[0] == null)
                return;

            if (iIndex == 0)
            {
                // make each book all around sphere with same distance.
                float magCameraDistance = magHit * 0.6f;
                Vector3 vecFaceDirection = transSphere.position + (targetPosArray[iIndex].position - transSphere.position).normalized * magCameraDistance;
                targetPosArray[iIndex].position = vecFaceDirection;
            }
            else
            {

                if ((targetPosArray[iIndex] != null) && (transSphere != null))
                {
                    targetPosArray[iIndex].position = targetPosArray[0].position;
                    // make book lean forward
                    targetPosArray[iIndex].LookAt(spotToLook, transSphere.up);
                }
            }

            targetPosArray[0].RotateAround(transSphere.position, transSphere.up, -fAngle);

            // make camera focus on the first book
            if ((targetPosArray[iIndex] != null) && (targetPosArray[iIndex].GetComponent<Book>().bookIndex == Book.CurrFrontIndex))
            {
                curTargetTrans = targetPosArray[iIndex];
                clockwise = (GetComponent<Camera>().WorldToViewportPoint(curTargetTrans.position).x == 0.5f) ? 0 :
                    (GetComponent<Camera>().WorldToViewportPoint(curTargetTrans.position).x < 0.5f) ? 1 : -1;
                
                bNeedAutoMove = true;
                actionPercent = 0.0f;
            }
        }

    }

    void LoadTexture()
    {
        //        string[] files = Directory.GetFiles(( Application.dataPath + "/Resources/Texture/").Replace("file://", ""), "*.*", SearchOption.TopDirectoryOnly);
        //        foreach(string f in files)
        //        {
        //            if (f.Contains("meta") || f.Contains("map"))// || !f.Contains("jpg"))
        //                continue;
        //            Texture2D texture = null;
        //            // here need to change as assetbundle version
        //            if (f.Contains("tga"))
        //            {
        //                texture = LoadTGA(f);
        //            }
        //            else if (f.Contains("jpg"))
        //            {
        //                WWW cache = new WWW("file://" + f);
        //                while (!cache.isDone)
        //                    texture = cache.texture;
        //                cache.Dispose();
        //            }
        int iCount = 0;
        string str = "Book/";//+ Path.GetFileNameWithoutExtension(f);
        Object[] objArray = Resources.LoadAll(str, typeof(Texture2D));
		while (objArray.Length < 8)
		{
			var tex = objArray[Random.Range(0, objArray.Length)];
			System.Array.Resize(ref objArray, objArray.Length + 1);
			objArray[objArray.Length - 1] = tex;
		}
        foreach (Object obj in objArray)
        {
            Texture2D texture = obj as Texture2D;

            // check done
            if (texture != null)
            {
                //if (External.CheckZoneTable(iCount))
                    listTexture.Add(iCount, texture);
                //else
                //    break;

                iCount++;
            }

        }

        System.Array.Clear(objArray, 0, objArray.Length);

    }

    public Texture2D GetTexture(int iIndex)
    {
        Texture2D textu = default(Texture2D);
        if (listTexture.ContainsKey(iIndex))
        {
            textu = listTexture[iIndex];
        }

        return textu;
    }

    public void DeSelect()
    {
        selectScrollItemEvent(TextureListCount * -1);
    }

    public void Select(int iIndex)
    {
        selectScrollItemEvent(iIndex);
    }

    public bool RotateTo(int iIndex)
    {
        if ((iIndex >= 0 && iIndex < TextureListCount) == false)
            return false;

        // modified by jackyli 2014/04/01 for repositioning back to zero
        // modified by jackyli 2020/11/27 replaced 1.0f by math constant
        if (actionPercent < Mathf.Epsilon && ((clockwise == 1) != (iIndex < Book.CurrFrontIndex)))
            return true;

        if (iIndex == Book.CurrFrontIndex)
        {
            if (actionPercent >= Mathf.Epsilon)
            {
                Select(iIndex);
                return false;
            }
            else
                return true;
        }


        bool bFound = false;
        int iNowObjectIndex = 0;
        for (int i = 0; i < targetPosArray.Length; ++i)
        {
            if (curTargetTrans.Equals(targetPosArray[i]))
                iNowObjectIndex = i;
            if (targetPosArray[i].GetComponent<Book>().bookIndex == iIndex)
            {
                curTargetTrans = targetPosArray[i];
                clockwise = iIndex < Book.CurrFrontIndex ? 1 : -1;

                bNeedAutoMove = true;
                actionPercent = 0.0f;

                bFound = true;
                break;
            }
        }

        // can't find match in present objects
        if (bFound == false)
        {
            // 2015/01/30 for flipping action optimize.
            if (Book.CurrFrontIndex != curTargetTrans.GetComponent<Book>().bookIndex)
                return true;

            int iIndexToGo = iIndex - Book.CurrFrontIndex;
            //iNowObjectIndex += (Mathf.Abs(iIndexToGo) < targetPosArray.Length) ? iIndexToGo : (iIndexToGo < 0) ? 1 : -1;
            iNowObjectIndex += _invisibleCount;
            if ((iNowObjectIndex >= 0 && iNowObjectIndex < targetPosArray.Length) == false)
                iNowObjectIndex += (iNowObjectIndex > 0) ? -targetPosArray.Length : targetPosArray.Length;

            {
                curTargetTrans = targetPosArray[iNowObjectIndex];

                clockwise = iIndexToGo < 0 ? 1 : -1;
                bNeedAutoMove = true;
                actionPercent = 0.0f;
            }
        }

        return true;
    }

    private int _invisibleCount = 0;
    public int InvisibleCount
    {
        get
        {
            return _invisibleCount;
        }
        private set
        {
            _invisibleCount = value;
        }
    }

	static public void showBook()
	{
        if (_instance == null)
            return;

        if (_instance.worldObj == null)
            _instance.worldObj = GameObject.Find("World");

        if (_instance.worldObj.activeSelf == false)
            _instance.worldObj.SetActive(true);

        _instance.gameObject.SetActive(true);

    }
	static public void hideBook(bool bBGShow = true, bool bBookShow = false)
	{
        if (_instance == null)
            return;

        if (_instance.worldObj == null)
            _instance.worldObj = GameObject.Find("World");

        if (_instance.worldObj.activeSelf)
            _instance.worldObj.SetActive(bBookShow);

        _instance.gameObject.SetActive(bBGShow);
    }
    #endregion

    #region MONOBEHAVIOUR


    void Awake()
    {
        if (_instance == null)
            _instance = this;

        if (transSphere == null)
            transSphere = GameObject.Find("Sphere").transform;

        magHit = Vector3.Distance(transSphere.position, transform.position);
        spotToLook = transSphere.position + Vector3.down * 5.0f;

        if (transform.Find("Meters") != null)
            transform.Find("Meters").GetComponent<Animation>().wrapMode = WrapMode.Loop;

        if (TextureListCount == 0)
            LoadTexture();

        //Transform tranBook = transSphere.parent.FindChild("Book");
        //for (int i = 1; i < 8; ++i)
        //{
        //    GameObject objbook = GameObject.Instantiate(tranBook.gameObject) as GameObject;
        //    objbook.GetComponent<Book>().bookIndex = i;
        //    objbook.transform.parent = tranBook.parent;
        //    objbook.transform.localScale = tranBook.localScale;
        //}
    }


    void Start()
    {
        SmoothLookAt(transSphere);
    }

    void OnDestroy()
    {
        System.Array.Clear(targetPosArray, 0, targetPosArray.Length);

        Dictionary<int, Texture2D>.Enumerator textureEn = listTexture.GetEnumerator();
        while(textureEn.MoveNext())
            Resources.UnloadAsset(textureEn.Current.Value);
        listTexture.Clear();

        System.Delegate[] delegList = selectScrollItemEvent.GetInvocationList();
        for (int i = 0; i < delegList.Length; ++i)
            selectScrollItemEvent -= (SelectScrollItemDelegate)delegList[i];
        // selectScrollItemEvent = null;

        _instance = null;

//        UjUtils.MemFree(UjUtils.eMemFree.all);
    }


    void Update()
    {
        if (GameInputHelper.GetTouchCount() > 1)
            return;

        if (bConstraintTouch)
            return;

        bIsTouchUp = GameInputHelper.IsTouchUp(0);
        bIsTouchDown = GameInputHelper.IsTouchDownTweak(0);
        bIsPressDown = GameInputHelper.IsKeepingTouch(0) && (GameInputHelper.GetTapCount() == 1);
        // Press down
        if (bIsTouchDown)
        {
            if (GameInputHelper.GetScreenPosition(0).x > 0 && 
                GameInputHelper.GetScreenPosition(0).x < Screen.width)
            {
                // block the situation of opened book
                if (CheckAnim() == true && (GameInputHelper.GetTapCount() <= 1))
                {
                    actionPercent = 1.0f;
                    RotateAndTranslateTo(curTargetTrans, clockwise);
                }

                lastTouchPos = GameInputHelper.GetScreenPosition(0);
                deltaTouchPos = lastTouchPos;
                curTouchPos = deltaTouchPos;
            }
        }
        // keep Press and Drag
        else if (bIsPressDown)
        {
            if (CheckAnim() == false)
            {
                curTouchPos = GameInputHelper.GetScreenPosition(0);

                fAmount = 0.0f;
                if ((curTouchPos.x > 0 && curTouchPos.x < Screen.width) == false)
                    curTouchPos = deltaTouchPos;

                clockwise = (lastTouchPos.x == curTouchPos.x) ? 0 : (lastTouchPos.x < curTouchPos.x) ? 1 : -1;
//                fAmount = ((curTouchPos.x - lastTouchPos.x) * 0.125f) / camera.nearClipPlane;

                deltaTouchPos = curTouchPos;
            }
        }
    }

    const string BOOK_LAYER = "Book";
    const string GUI_LAYER = "2dGUI";

    void LateUpdate()
    {
        bConstraintTouch = External.Check2D() || CheckAnim(false);

        if (GameInputHelper.GetTouchCount() > 1)
            return;

        if (bIsTouchUp == false && bIsPressDown == true)
        {
            if (Book.CheckSelectedIndex(clockwise))
            {
                deltaTouchPos.x -= fAmount;
                
                actionPercent = 1.0f;
                RotateAndTranslateTo(null, clockwise);
                
                deltaTouchPos = curTouchPos;
                return;
            }
            else
            {
                int iIndexNow = System.Array.FindIndex(targetPosArray, tran => tran.GetComponent<Book>().bookIndex == Book.CurrFrontIndex);
                curTargetTrans = targetPosArray[iIndexNow];
                actionPercent = 1.0f;
                RotateAndTranslateTo(curTargetTrans, clockwise);
            }
        }

        if (bIsTouchUp)
        {
            // the hitting collider
            RaycastHit hit;
            Ray ray;
            // find front book index
            int iIndexNow = System.Array.FindIndex(targetPosArray, tran => tran.GetComponent<Book>().bookIndex == Book.CurrFrontIndex);
            int iNext = iIndexNow + (-clockwise);
            if (iNext < 0) iNext = BookCount - 1;
            if (iNext > BookCount - 1) iNext = 0;

            // calculate if drag
            float fDragDistance = (curTouchPos - lastTouchPos).magnitude;
            // if drag end
            if (fDragDistance > 30.0f)
            {
                bNeedAutoMove = true;
                actionPercent = 0.0f;
                if (Book.CheckSelectedIndex(clockwise) 
                    && (targetPosArray[iNext].GetComponent<Collider>().isTrigger == true))
                    curTargetTrans = targetPosArray[iNext];
                else
                {
                    curTargetTrans = targetPosArray[iIndexNow];
                    clockwise = GetComponent<Camera>().WorldToViewportPoint(curTargetTrans.position).x == 0.5f ? 0 :
                        GetComponent<Camera>().WorldToViewportPoint(curTargetTrans.position).x < 0.5f ? 1 : -1;
                    actionPercent = 1.0f;
                }
            }
            // if not drag
            else
            {
                ray = GetComponent<Camera>().ScreenPointToRay(new Vector3(curTouchPos.x, curTouchPos.y, GetComponent<Camera>().nearClipPlane));
                bNeedAutoMove = Physics.Raycast(ray, out hit, magHit, 1 << LayerMask.NameToLayer(BOOK_LAYER));

                if (bNeedAutoMove == true && CheckAnim() == false
                    && (hit.collider.isTrigger == true))
                {
                    actionPercent = 0.0f;
                    Transform hitTrans = hit.collider.transform;
                    clockwise = GetComponent<Camera>().WorldToViewportPoint(hitTrans.position).x == 0.5f ? 0 :
                        GetComponent<Camera>().WorldToViewportPoint(hitTrans.position).x < 0.5f ? 1 : -1;

                    if (hitTrans.Equals(curTargetTrans))
                    {
                        // if (selectScrollItemEvent != null)
                            selectScrollItemEvent(curTargetTrans.GetComponent<Book>().bookIndex);

                        bNeedAutoMove = clockwise != 0 ? true : false;
                        actionPercent = bNeedAutoMove ? 0.0f : 1.0f;
                    }

                    curTargetTrans = hitTrans;
                }
                else
                {
                    curTargetTrans = targetPosArray[iIndexNow];
                    clockwise = GetComponent<Camera>().WorldToViewportPoint(curTargetTrans.position).x == 0.5f ? 0 :
                        GetComponent<Camera>().WorldToViewportPoint(curTargetTrans.position).x < 0.5f ? 1 : -1;
                    bNeedAutoMove = clockwise != 0 ? true : false;
                    actionPercent = bNeedAutoMove ? 0.0f : 1.0f;
                }

            }
        }

        if (bNeedAutoMove)
        {
            if (actionPercent < 1.0f)
                actionPercent += Time.deltaTime * 1.0f;

            RotateAndTranslateTo(curTargetTrans, clockwise);
        }

    }

    void OnGUI()
    {
        if (External.GetMainProcMgr() != null)
            return;

        if (GUILayout.Button("back",  GUILayout.Width(150),  GUILayout.Height(75)))
        {
            // all close
            DeSelect();
        }

        if (GUILayout.Button("exit", GUILayout.Width(150), GUILayout.Height(75)))
        {
            Application.Quit();
        }
    }
    #endregion


    #region CAMERA_ACTION
    void RotateAndTranslateTo(Transform target, int iclockwise = 0)
    {
        float fAngle = 0.0f;
        if (target)
        {
            //adjust rolling speed
            fAngle = Vector3.Angle(target.position - transSphere.position, transform.position - transSphere.position);
            if (fAngle >= 0.1f)
                fAngle *= actionPercent;
            else
            {
                bNeedAutoMove = false;
                actionPercent = 1.0f;
                clockwise = 0;
            }
        }
        else
        {
            Vector3 deltaPos = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(deltaTouchPos.x, Screen.height * 0.5f, GetComponent<Camera>().nearClipPlane));
            Vector3 dragPos = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(curTouchPos.x, Screen.height * 0.5f, GetComponent<Camera>().nearClipPlane));
            fAngle = Vector3.Angle(deltaPos - transSphere.position, dragPos - transSphere.position) * GetComponent<Camera>().nearClipPlane;
            // drag speed up plus 50%
            //fAngle *= 1.5f;
        }


        fAngle *= iclockwise;
        // rotate camera itself
        transform.RotateAround(transSphere.position, CameraRelativeUp, fAngle);
        // rotate the books
        //transSphere.parent.Rotate(transSphere.up, fAngle, Space.Self);
    }

    void SmoothMoveTo(Transform target)
    {
        //if (fromVec.Equals(Vector3.zero) && toVec.Equals(Vector3.zero))
        if (target)
        {
            Vector3 fromVec = transform.position;
            Vector3 toVec = new Vector3(target.position.x, fromVec.y, fromVec.z);


            // cannot use Slerp for the strange move
            transform.position = Vector3.Lerp(fromVec, toVec, actionPercent);
        }
    }

    void SmoothLookAt(Transform target)
    {
        if (target)
        {
            // Look at and dampen the rotation in smooth way
            //Quaternion rotation = Quaternion.LookRotation(target.position - transform.position, CameraRelativeUp);
            //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, actionPercent);

            transform.LookAt(target, CameraRelativeUp);
        }
    }

    #endregion

    #region CHECK_FUNCTION
    float CheckMinAngle(out Transform tTrans, out int iclockwise)
    {
        Vector3 vecCam = transform.forward;
        // make it the biggest so that there can only be the min
        float fAngle = 180.0f;

        Transform tempTrans = null;
        for (int i = 0 ; i < BookCount ; ++i)
        {

            if (targetPosArray[i] == null)
                continue;
            // comment by jackyli 2013/12/16
            // this vector has a little deviation about leaning forward.
            // yet doesn't matter when calculate the min angle differences.
            Vector3 vectemp = transSphere.position - targetPosArray[i].position;
            float ftempAngle = Vector3.Angle(vecCam, vectemp);
            if (ftempAngle > Mathf.Epsilon && ftempAngle < fAngle)
            {
                if (targetPosArray[i].GetComponent<Collider>().isTrigger == false)
                {
                    // set the force acceleration to 0 ?
                    //tempTrans = curTargetTrans;
                    continue;
                }

                fAngle = ftempAngle;
                tempTrans = targetPosArray[i];
            }

        }

        tTrans = tempTrans;
        iclockwise = (GetComponent<Camera>().WorldToViewportPoint(tTrans.position).x == 0.5f) ? 0 :
            (GetComponent<Camera>().WorldToViewportPoint(tTrans.position).x < 0.5f) ? 1 : -1;

        return fAngle;
    }

    public bool CheckAnim(bool bCheckFlip = true)
    {
        bool bIsAnim = false;
        for (int i = 0; i < targetPosArray.Length; ++i)
        {
            // check if book opened as well
            bIsAnim |= targetPosArray[i].GetComponent<Book>().CheckAnim;
        }
        if (bCheckFlip)
		{
			bIsAnim |= (actionPercent < 1.0f);
			if (worldObj != null)
				bIsAnim |= (worldObj.activeSelf == false);
		}
        return bIsAnim;
    }
    #endregion


}


