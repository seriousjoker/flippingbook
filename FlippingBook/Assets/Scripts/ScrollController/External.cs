﻿//#define ExternalMain
//
//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//
//
//public sealed class External
//{
//
//    static public bool Check2D()
//    {
//        // 教學時，回傳true，讓翻書功能(ScrollController)停止接收玩家的input
//        if (UITutorialMgr.Instance != null && UITutorialMgr.Instance.IsPlayingScript)
//            return true;
//
//        if (UICamera.hoveredObject == null)
//            return false;
//
//        bool bFlipping = Main.Instance == null ? false : Main.Instance.IkContain("ProcSelectZone");
//
//        return bFlipping | ((Main.Instance == null) ? false :
//            (UICamera.hoveredObject == UICamera.fallThrough) ? false :
//                UICamera.hoveredObject.activeSelf);
//    }
//
//    public static object GetMain()
//    {
//        return Main.Instance;
//    }
//
//    public static object GetMainProcMgr()
//    {
//        return Main.procMgr;
//    }
//
//    [System.Diagnostics.Conditional("ExternalMain")]
//    public static void ChangeChapter(int iIndex)
//    {
//        if (Main.Instance != null)
//        {
//            UjShared.WndBase wnd = Main.procMgr.curProc.GetWnd(DefWndNameID.UI_ZONETITLE);
//            if (wnd != null && Main.procMgr.curProc.bCreateFinish == true)
//                wnd.GetType().GetMethod("SetZoneTitle").Invoke(wnd, new object[]{iIndex + 1});
//        }
//    }
//
//    [System.Diagnostics.Conditional("ExternalMain")]
//    public static void Action(bool isOpen, int iIndex)
//    {
//        if (Main.procMgr != null)
//        {
//            UjShared.ProcBase proc = Main.procMgr.getCurProc();
//            if (proc != null)
//            {
//				if (isOpen && proc.visible == true)
//				{
//					if (proc.GetType().GetMethod("openBook") != null)
//						proc.GetType().GetMethod("openBook").Invoke(proc, new object[] { iIndex + 1 });
//				}
//                //else
//                //    proc.GetType().GetMethod("closeBook").Invoke(proc, new object[]{ bookIndex + 1});
//            }
//        }
//    }
//
//    public static bool Select(int iIndex)
//    {
//        bool bValid = true;
//        if (Main.Instance != null)
//        {
//            if (Main.Instance.zoneDataController.onSelectZone((uint)iIndex + 1, true) != ZoneDataController.SELECT_ZONE_RESULT.RESULT_OK)
//                bValid = false;
//            if (bValid)
//                Main.procMgr.getCurProc().playDefaultSoundHandler(new aNguiEvent(null, NguiEventType.onHover, null));
//        }
//        return bValid;
//    }
//
//    public static bool CheckZoneTable(int iZone)
//    {
//        // Check ZoneTable
//        if (Main.Instance == null)
//            return true;
//        else if (Main.Instance.IsClientTesting)
//            return true;
//        else if (TableManager.dictZone2SubZone.ContainsKey(iZone + 1) && (iZone + 1 <= System.Convert.ToInt32(TableManager.GetItem<FormulaTable, FormulaRow>(435).param1)))
//            return true;
//        else
//            return false;
//    }
//}
