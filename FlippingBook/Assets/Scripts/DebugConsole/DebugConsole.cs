﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class DebugConsole : UjShared.DebugConsoleBaseBehaviour
{
    #region Static instance of the console.
    private static DebugConsole _instance = null;
    public static DebugConsole Instance 
	{
        get 
		{
            if (_instance == null) 
			{
                _instance = FindObjectOfType(typeof(DebugConsole)) as DebugConsole;
            }
            return _instance;
        }
    }
    #endregion

    protected override void on_Awake() 
	{
        _instance = this;

        // 以下可以註冊 gm 指令
		
//		this.RegisterCommandCallback("getmaillist", GmInstruction.Instance.getMailList);
//		this.RegisterCommandCallback("getinvitelist", GmInstruction.Instance.getInviteList);
//		this.RegisterCommandCallback("getnoticelist", GmInstruction.Instance.getNoticeList);
//		this.RegisterCommandCallback("gettemplist", GmInstruction.Instance.getTemporaryList);
    }
	
	protected override bool CheckClientGMInstruction(string inputString)
	{
		char[] seperators = { ' ' };
        string[] input = inputString.Split(seperators);

        string[] args = new string[input.Length - 1];

        for (int x = 0; x < args.Length; x++)
        {
            args[x] = input[x + 1];
        }

        if (_cmdTable.ContainsKey(input[0].ToLower()))
        {
			Debug.Log("GM Instruction:" + inputString);
			
            ((DebugCommand)_cmdTable[input[0].ToLower()]).Invoke(args);
            
			return true;
        }
		
		return false;
	}
}

