﻿namespace UjShared
{

    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;

    public abstract class DebugConsoleBaseBehaviour : MonoBehaviour
    {
        public delegate void DebugCommand(params string[] args);

        // This Enum holds the message types used to easily control the formatting and display of a message.
        public enum MessageTypes { Normal, Error, Warning, Command, System };

        // Display modes for the console.
        public enum Mode { None, Log, CopyLog, WatchVars };

        #region 訊息接收函式(GameObject.SendMessage)
        void onWindowSizeChange()						// 視窗大小改變時呼叫
        {
            //Debug.Log("DebugConsole:onWindowSizeChange");
            _wndSize = new Vector2(Screen.width * 0.8f, Screen.height * 0.8f);
            _wndRect = new Rect((Screen.width - _wndSize[0]) * 0.5f, 0, _wndSize[0], _wndSize[1]);
        }
        #endregion
        #region attributes

        // How many lines of text this console will display.
        int _maxLinesForDisplay = 32;

        // Default color of the standard display text.
        Color _defaultTextColor = Color.white;

        // Used to check (or toggle) the open state of the console.
        public static bool IsOpen {
            get { return DebugConsole.Instance._isOpen; }
            set {
                DebugConsole.Instance._isOpen = value;
                if (value) {
                    if (Mode.None == DebugConsole.Instance._mode)
                        DebugConsole.Instance._mode = Mode.Log;
                }
                else {
                    DebugConsole.Instance._mode = Mode.None;
                }
            }
        }

        public static Mode DisplayMode {
            get { return DebugConsole.Instance._mode; }
        }

        // Represents a single message, with formatting options.
        private struct Message
        {
            public string MessageText {
                get { return _messageText; }
                set { _messageText = value; }
            }

            public MessageTypes MessageType {
                get { return _messageType; }
                set { _messageType = value; }
            }

            public Color DisplayColor {
                get { return _displayColor; }
                set { _displayColor = value; }
            }

            public bool UseCustomColor {
                get { return _useCustomColor; }
                set { _useCustomColor = value; }
            }

            private string _messageText;
            private MessageTypes _messageType;
            private Color _displayColor;
            private bool _useCustomColor;

            public Message(string messageText) {
                this._messageText = messageText;
                this._messageType = MessageTypes.Normal;
                this._displayColor = new Color();
                this._useCustomColor = false;
            }

            public Message(string messageText, MessageTypes messageType) {
                this._messageText = messageText;
                this._messageType = messageType;
                this._displayColor = new Color();
                this._useCustomColor = false;
            }

            public Message(string messageText, MessageTypes messageType, Color displayColor) {
                this._messageText = messageText;
                this._messageType = messageType;
                this._displayColor = displayColor;
                this._useCustomColor = true;
            }

            public Message(string messageText, Color displayColor) {
                this._messageText = messageText;
                this._messageType = MessageTypes.Normal;
                this._displayColor = displayColor;
                this._useCustomColor = true;
            }
        }
        private Queue<Message> _messages;
        private Queue<Message> _messagesBack;

        private Mode _mode = Mode.None;
        protected Hashtable _cmdTable;
        private SortedList<string, WatchVar> _watchVarTable;
        private StringBuilder _displayString;
        private string _inputString = "";

        #region private attribute of 顯示
        private bool _slide_busy = false;
        private Rect _slide_rect;
        private float _slide_time;
        private float _slide_elapse_time;
        private Vector2 _wndSize;
        private Rect _wndRect;
        private Vector2 _logScrollPos = Vector2.zero;
        private Vector2 _watchVarsScrollPos = Vector2.zero;
        private bool _isOpen = false;
        private string _buildTime = "";
        #endregion
        #endregion
        #region Behaviour::Awake
        void Awake() {

            _messages = new Queue<Message>();
            _messagesBack = new Queue<Message>();
            _cmdTable = new Hashtable();
            _watchVarTable = new SortedList<string, WatchVar>();
            _displayString = new StringBuilder();

            // 初始化座標
            this.onWindowSizeChange();

            this.RegisterCommandCallback("close", CMDClose);
            this.RegisterCommandCallback("clear", CMDClear);
			
			on_Awake();
        }
        protected abstract void on_Awake();
        #endregion
        #region StaticAccessors

        /// <summary>
        /// Prints a message string to the console.
        /// </summary>
        /// <param name="text">Message to print.</param>
        public static void Log(string text) {
            DebugConsole.Instance.LogMessage(text);
        }

        /// <summary>
        /// Prints a message string to the console.
        /// </summary>
        /// <param name="text">Message to print.</param>
        /// <param name="messageType">The MessageType of the message. Used to provide 
        /// formatting in order to distinguish between message types.</param>
        public static void Log(string text, MessageTypes messageType) {
            DebugConsole.Instance.LogMessage(text, messageType);
        }

        /// <summary>
        /// Prints a message string to the console.
        /// </summary>
        /// <param name="text">Message to print.</param>
        /// <param name="displayColor">The text color to use when displaying the message.</param>
        public static void Log(string text, Color displayColor) {
            DebugConsole.Instance.LogMessage(text, displayColor);
        }

        /// <summary>
        /// Prints a message string to the console.
        /// </summary>
        /// <param name="text">Messate to print.</param>
        /// <param name="messageType">The MessageType of the message. Used to provide 
        /// formatting in order to distinguish between message types.</param>
        /// <param name="displayColor">The color to use when displaying the message.</param>
        /// <param name="useCustomColor">Flag indicating if the displayColor value should be used or 
        /// if the default color for the message type should be used instead.</param>
        public static void Log(string text, MessageTypes messageType, Color displayColor, bool useCustomColor) {
            DebugConsole.Instance.LogMessage(text, messageType, displayColor, useCustomColor);
        }

        /// <summary>
        /// Prints a message string to the console using the "Warning" message type formatting.
        /// </summary>
        /// <param name="text">Message to print.</param>
        public static void LogWarning(string text) {
            DebugConsole.Instance.LogMessage(text, MessageTypes.Warning);
        }

        /// <summary>
        /// Prints a message string to the console using the "Error" message type formatting.
        /// </summary>
        /// <param name="text">Message to print.</param>
        public static void LogError(string text) {
            DebugConsole.Instance.LogMessage(text, MessageTypes.Error);
        }

        /// <summary>
        /// Clears all console output.
        /// </summary>
        public static void Clear() {
            DebugConsole.Instance.ClearLog();
        }

        /// <summary>
        /// Registers a debug command that is "fired" when the specified command string is entered.
        /// </summary>
        /// <param name="commandString">The string that represents the command. For example: "FOV"</param>
        /// <param name="commandCallback">The method/function to call with the commandString is entered. 
        /// For example: "SetFOV"</param>
        public static void RegisterCommand(string commandString, DebugCommand commandCallback) {
            DebugConsole.Instance.RegisterCommandCallback(commandString, commandCallback);
        }

        /// <summary>
        /// Removes a previously-registered debug command.
        /// </summary>
        /// <param name="commandString">The string that represents the command.</param>
        public static void UnRegisterCommand(string commandString) {
            DebugConsole.Instance.UnRegisterCommandCallback(commandString);
        }

        /// <summary>
        /// Registers a named "watch var" for monitoring.
        /// </summary>
        /// <param name="name">Name of the watch var to be shown in the console.</param>
        /// <param name="watchVar">The WatchVar instance you want to monitor.</param>
        public static void RegisterWatchVar(string name, WatchVar watchVar) {
            DebugConsole.Instance.AddWatchVarToTable(name, watchVar);
        }

        /// <summary>
        /// Removes a previously-registered watch var.
        /// </summary>
        /// <param name="name">Name of the watch var you wish to remove.</param>
        public static void UnRegisterWatchVar(string name) {
            DebugConsole.Instance.RemoveWatchVarFromTable(name);
        }

        //==== Built-in example DebugCommand handlers ====
        public void CMDClose(params string[] args) {
            _isOpen = false;
        }

        public void CMDClear(params string[] args) {
            this.ClearLog();
        }

        // 開啟對話
        public void CMDTalk(params string[] args) {
            if (1 != args.Length)
                return;

            Log("cmd talk, " + args[0]);
        }

        #endregion
        #region 顯示相關介面流程

        public bool switch_mode(DebugConsole.Mode mode) {
            if (!_slide_busy) {

                bool hide = false;

                if (_mode != mode) {
                    _mode = mode;
                    this.slide_reset();
                }
                else {
                    hide = true;
                }

                if (_mode == Mode.None || hide) {
                    DebugConsole.IsOpen = false;
                }
                else {
                    DebugConsole.IsOpen = true;
                }

                return true;
            }
            else {
                return false;
            }
        }

        void Update() {
//            if (gDefines.GameMode == gDefines.eGameMode.Develop || gDefines.IsGM) {
                #region ~ + D 顯示 Console
                // ~ + D 顯示 Console
                // ~ + S 顯示 Watcher
                if (Input.GetKey(KeyCode.BackQuote)) {
                    // toggle debug console
                    if (Input.GetKeyDown(KeyCode.D)) {
                        this.switch_mode(DebugConsole.Mode.Log);
                    }
                    // toggle game realtime stats
                    if (Input.GetKeyDown(KeyCode.S)) {
                        this.switch_mode(DebugConsole.Mode.WatchVars);
                    }
                }
                #endregion
                #region 手勢
                // 平板下開啟偵錯視窗
                if (Input.touchCount > 0) {
                    Touch touch0 = Input.touches[0];

                    //Debug.Log(string.Format("{0},{1},tap={2},phase={3},deltaT={4},deltaP={5},w={6},h={7}",
                    //    touch0.fingerId, touch0.position, touch0.tapCount, touch0.phase, touch0.deltaTime, touch0.deltaPosition, Screen.width, Screen.height));

                    /* 將操作的座標投影
                      (-1,  1) ┌───┬───┐ ( 1,  1)
                               │   │   │  
                      (-1,  0) ├───┼───┤ ( 1,  0)
                               │   │   │
                      (-1, -1) └───┴───┘ ( 1, -1)
                     * 
                     */
                    float real_x = touch0.position.x;
                    float real_y = touch0.position.y;
                    int sw = Screen.width;
                    int sh = Screen.height;
                    float t_x = (2.0f * real_x) / sw - 1.0f;
                    float t_y = (2.0f * real_y) / sh - 1.0f;
                    float speed = 0.4f;

                    //Debug.Log( string.Format("({0}, {1})", t_x, t_y) );

                    if (!_isOpen) {
                        if (t_y > 0.8f && t_x > 0.9f) {
                            if (touch0.deltaPosition.y >= -speed) {      // 往下滑
                                Debug.Log(string.Format("DebugConsole:Gesture: Mode.Log"));
                                // 顯示 Log
                                this.switch_mode(DebugConsole.Mode.Log);
                            }
                        }
                    }
                    else {
                        if (t_y > 0.8f && t_x > 0.9f) {
                            if (touch0.deltaPosition.y <= -speed) {     // 往下滑
                                // Log 與 watcher 輪流顯示
                                if (DebugConsole.DisplayMode == DebugConsole.Mode.Log) {
                                    if (this.switch_mode(DebugConsole.Mode.WatchVars)) {
                                        //Debug.Log(string.Format("DebugConsole:Gesture: Mode.WatchVars"));
                                    }
                                }
                                else if (DebugConsole.DisplayMode == DebugConsole.Mode.WatchVars) {
                                    if (this.switch_mode(DebugConsole.Mode.Log)) {
                                        //Debug.Log(string.Format("DebugConsole:Gesture: Mode.Log"));
                                    }
                                }
                                else {
                                    if (this.switch_mode(DebugConsole.Mode.Log)) {
                                        //Debug.Log(string.Format("DebugConsole:Gesture: Mode.Log"));
                                    }
                                }
                            }
                            else if (touch0.deltaPosition.y >= speed) {      // 往上滑
                                // 關閉
                                if (this.switch_mode(DebugConsole.Mode.None)) {
                                    //Debug.Log(string.Format("DebugConsole:Gesture: Close"));
                                }
                            }
                        }
                    }
                }
                this.slide_tick();
                #endregion

//            }
        }

        #region 視窗滑入與滑出
        private void slide_reset() {
            _slide_busy = true;
            _slide_rect = new Rect(_wndRect);
            _slide_elapse_time = _slide_time = 200.0f;

            float deltaY = _wndRect.yMax * (1.0f - (_slide_time - _slide_elapse_time) / _slide_time);
            _slide_rect.yMin = _wndRect.yMin - deltaY;
            _slide_rect.yMax = _wndRect.yMax - deltaY;
        }

        private void slide_tick() {

            if (_slide_busy) {

                float deltaY = _wndRect.yMax * (1.0f - (_slide_time - _slide_elapse_time) / _slide_time);
                _slide_rect.yMin = _wndRect.yMin - deltaY;
                _slide_rect.yMax = _wndRect.yMax - deltaY;

                _slide_elapse_time -= Time.deltaTime * 1000.0f;
                if (_slide_elapse_time <= 0) {
                    _slide_busy = false;
                    _slide_rect.yMin = _wndRect.yMin;
                    _slide_rect.yMax = _wndRect.yMax;
                }

            }
        }
        #endregion
        void OnGUI() {
            if (_isOpen) {
                // 取得編譯時間
                if (_buildTime == string.Empty) {
                    _buildTime = string.Format(" (compiled at {0})", Time.time);
                }

                _slide_rect = GUI.Window(-1111, _slide_rect, MainWindow, "Debug Console" + _buildTime);
                GUI.BringWindowToFront(-1111);

                // 讓 DebugConsole 能使用鍵盤 Enter 做指令輸入
                if (GUI.GetNameOfFocusedControl() == "DebugConsole/MainWindow/Mode.Log/inputString") {
                    if (Event.current.type == EventType.KeyDown && Event.current.character == '\n') {
                        EvalInputString(_inputString);
                        _inputString = "";
                    }
                }

                // focus 在 copyLogArea 時會自動全選功能
                //else if (GUI.GetNameOfFocusedControl() == "DebugConsole/MainWindow/Mode.Log/copyLogArea")
                //{
                //    TextEditor t = GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl) as TextEditor;
                //    t.SelectAll();
                //}
            }
        }

        private void MainWindow(int windowID) {
            if (_mode == Mode.Log) {
                GUILayout.BeginHorizontal();

                GUILayout.BeginVertical();

                //GUILayout.Label( "Normal Log:" );

                _logScrollPos = GUILayout.BeginScrollView(_logScrollPos);

                DisplayNormalLog();

                GUILayout.EndScrollView();

                GUILayout.Space(4.0F);

                GUILayout.BeginHorizontal();
                GUI.SetNextControlName("DebugConsole/MainWindow/Mode.Log/inputString");
                _inputString = GUILayout.TextField(_inputString);

                if (GUILayout.Button("Enter", GUILayout.Width(70.0F), GUILayout.Height(20.0F))) {
                    EvalInputString(_inputString);
                    _inputString = "";
                }

                GUILayout.EndHorizontal();

                GUILayout.Space(4.0F);

                GUILayout.BeginHorizontal();

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Log Window", GUILayout.Width(128.0F), GUILayout.Height(23.0F))) {
                    // 切換到 CopyLog 模式
                    _mode = Mode.CopyLog;

                    /*string ret = "";
                    foreach (string text in _messagesBack)
                    {
                        ret += string.Format("{0} \n", text);
                    }
                    Debug.Log("Copy Log");
                    //EditorGUIUtility.systemCopyBuffer = ret;
                    Application.ExternalCall("setClipBoardData", ret);*/
                }
				
                if (GUILayout.Button("Enter", GUILayout.Width(70.0F), GUILayout.Height(20.0F))) {
                    EvalInputString(_inputString);
                    _inputString = "";
                }

                //if ( GUILayout.Button( "Watch Vars", GUILayout.Width( 95.0F ), GUILayout.Height( 20.0F ) ) )
                //{
                //    _mode = Mode.WatchVars;
                //}

                GUILayout.FlexibleSpace();

                GUILayout.EndHorizontal();

                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }
            else if (_mode == Mode.CopyLog) {
                GUILayout.BeginHorizontal();
                {
                    GUILayout.Space(5.0F);

                    GUILayout.BeginVertical();
                    {
                        GUILayout.Space(5.0F);

                        GUILayout.Label("Log Histroy:");
                        _logScrollPos = GUILayout.BeginScrollView(_logScrollPos);

                        BuildDisplayString();

                        GUI.SetNextControlName("DebugConsole/MainWindow/Mode.Log/copyLogArea");

                        GUILayout.TextArea(_displayString.ToString(), GUILayout.ExpandWidth(true),
                                                GUILayout.ExpandHeight(true), GUILayout.Width(_wndSize[0]));

                        GUILayout.EndScrollView();

                        GUILayout.Space(4.0F);

                        GUILayout.BeginHorizontal();

                        GUILayout.FlexibleSpace();

                        if (GUILayout.Button("Copy Log", GUILayout.Width(128.0F), GUILayout.Height(23.0F))) {
                            GUI.FocusControl("DebugConsole/MainWindow/Mode.Log/copyLogArea");
                            TextEditor t = GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl) as TextEditor;
                            t.SelectAll();
                        }

                        if (GUILayout.Button("Normal Mode", GUILayout.Width(128.0F), GUILayout.Height(23.0F))) {
                            _mode = Mode.Log;
                        }

                        //if ( GUILayout.Button( "Watch Vars", GUILayout.Width( 95.0F ), GUILayout.Height( 20.0F ) ) )
                        //{
                        //    _mode = Mode.WatchVars;
                        //}

                        //GUILayout.FlexibleSpace();

                        GUILayout.EndHorizontal();
                    }
                    GUILayout.EndVertical();
                }
                GUILayout.EndHorizontal();
            }
            else if (_mode == Mode.WatchVars) {
                GUILayout.BeginHorizontal();
                {
                    GUILayout.Space(2.0F);
                    GUILayout.BeginVertical();
                    {
                        GUILayout.Space(2.0F);
                        GUILayout.Label("Watched Vars:");
                        _watchVarsScrollPos = GUILayout.BeginScrollView(_watchVarsScrollPos);
                        {
                            foreach (KeyValuePair<string, WatchVar> item in _watchVarTable) {
                                //GUILayout.Space(1.0F);
                                GUILayout.BeginHorizontal();
                                {
                                    GUILayout.Label(item.Value.Name + ": ");
                                    GUILayout.FlexibleSpace();
                                    GUILayout.Label(item.Value.GetValue());
                                    GUILayout.Space(1.0F);
                                }
                                GUILayout.EndHorizontal();
                            }
                        }
                        GUILayout.EndScrollView();
                        GUILayout.Space(4.0F);
                    }
                    GUILayout.EndVertical();
                }
                GUILayout.EndHorizontal();
            }

            // web 偵錯視窗不需要拖動, mobile 例外
            //if (UjApplicationSetting.UiStyle == UiStyle.Mobile) {
            //    GUI.DragWindow();
            //}
        }
        private void DisplayNormalLog() {
            if (_messages != null && _messages.Count > 0) {
                foreach (Message m in _messages) {
                    // Default text color                
                    Color displayColor = _defaultTextColor;

                    if (m.UseCustomColor) {
                        displayColor = m.DisplayColor;
                    }
                    else if (m.MessageType == MessageTypes.Error) {
                        displayColor = Color.red;
                    }
                    else if (m.MessageType == MessageTypes.Warning) {
                        displayColor = Color.yellow;
                    }
                    else if (m.MessageType == MessageTypes.System) {
                        displayColor = Color.green;
                    }
                    else if (m.MessageType == MessageTypes.Command) {
                        displayColor = Color.magenta;
                    }

                    Color oldColor = GUI.color;
                    GUI.color = displayColor;
                    GUILayout.Label(">> " + m.MessageText);
                    GUI.color = oldColor;
                }
            }
        }
        private void BuildDisplayString() {
            if (_messagesBack != null && _messagesBack.Count > 0) {
                _displayString = new StringBuilder();

                foreach (Message m in _messagesBack) {
                    string messageTypeString = "";

                    if (m.UseCustomColor == false) {
                        if (m.MessageType == MessageTypes.Error) {
                            messageTypeString = "error";
                        }
                        else if (m.MessageType == MessageTypes.Warning) {
                            messageTypeString = "warning";
                        }
                        else if (m.MessageType == MessageTypes.System) {
                            messageTypeString = "system";
                        }
                        else if (m.MessageType == MessageTypes.Command) {
                            messageTypeString = "command";
                        }
                    }
                    else {
                        messageTypeString = "customColor(" + m.DisplayColor.ToString() + ")";
                    }

                    if (!string.IsNullOrEmpty(messageTypeString)) {
                        _displayString.AppendLine(">> [" + messageTypeString + "]" + m.MessageText + "[/" + messageTypeString + "]");
                    }
                    else {
                        _displayString.AppendLine(">> " + m.MessageText);
                    }
                }
            }
        }
        private void EvalInputString(string inputString) {
			if (CheckClientGMInstruction(inputString) == false)
			{
//				using (UjShared.BatchCmdDeliver obj = new UjShared.BatchCmdDeliver(eNetCmd.GmInstruction, false)) {
//	                obj.JsonSet["cmdstr"] = inputString;
//            	}
			} 
        }
        #endregion
        #region Log, Command, Watch API
        public void LogMessage(string text) {
            LogMessage(text, MessageTypes.Normal, Color.white, false);
        }
        public void LogMessage(string text, MessageTypes messageType) {
            LogMessage(text, messageType, Color.white, false);
        }
        public void LogMessage(string text, Color displayColor) {
            LogMessage(text, MessageTypes.Normal, displayColor, true);
        }
        public void LogMessage(string text, MessageTypes messageType, Color displayColor, bool useCustomColor) {
            if (_messages != null) {
                if (useCustomColor) {
                    _messages.Enqueue(new Message(text, messageType, displayColor));
                    _messagesBack.Enqueue(new Message(text, messageType, displayColor));
                }
                else {
                    _messages.Enqueue(new Message(text, messageType));
                    _messagesBack.Enqueue(new Message(text, messageType));
                }

                // 不要讓紀錄無限制一直成長
                if (_messages.Count >= _maxLinesForDisplay) {
                    _messages.Dequeue();
                }

                _logScrollPos = new Vector2(_logScrollPos.x, 50000.0F);
            }
        }
        public void ClearLog() {
            _messages.Clear();
            _messagesBack.Clear();
        }
        public void RegisterCommandCallback(string commandString, DebugCommand commandCallback) {
            _cmdTable.Add(commandString.ToLower(), new DebugCommand(commandCallback));
        }
        public void UnRegisterCommandCallback(string commandString) {
            _cmdTable.Remove(commandString);
        }
        public void AddWatchVarToTable(string name, WatchVar watchVar) {
            if (!_watchVarTable.ContainsKey(name)) {
                _watchVarTable.Add(name, watchVar);
            }
        }
        public void RemoveWatchVarFromTable(string name) {
            if (_watchVarTable.ContainsKey(name)) {
                _watchVarTable.Remove(name);
            }
        }
        #endregion
		
		#region 過濾client GM指令
		virtual protected bool CheckClientGMInstruction(string inputString)
		{
			return false;
		}
		#endregion
    }

    #region Watchers

    public abstract class WatchVar
    {
        public string Name {
            get { return _name; }
            set { _name = value; }
        }

        private string _name = "Default WatchVar";

        public WatchVar(string name) {
            this._name = name;
            DebugConsole.RegisterWatchVar(_name, this);
        }

        public abstract string GetValue();
    }

    public class WatchType<T> : WatchVar
    {
        private T _value;

        public T Value {
            get { return _value; }
            set { _value = value; }
        }

        public WatchType(string name, T defValue)
            : base(name) {
            Value = defValue;
        }

        public override string GetValue() {
            return _value.ToString();
        }
    }

    #endregion

}