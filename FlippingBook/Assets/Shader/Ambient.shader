Shader "Ambient" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
}

SubShader {
	Tags { "RenderType"="Opaque" "Queue"="Geometry" }
	LOD 100
	
	Pass {
		ColorMaterial AmbientAndDiffuse 
		 Material {
			Diffuse (1,1,1,1)
			Ambient (1,1,1,1)
		}
		Lighting On
		ZWrite On
		Offset 0,10
		SetTexture [_MainTex] { Combine texture * primary Double} 
	}
}
}
