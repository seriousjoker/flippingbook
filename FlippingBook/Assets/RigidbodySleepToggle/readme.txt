Rigidbody Sleep Toggle
----------------------

Once added to your project all Rigidbody component will now have a toggle called 'Sleep On Awake'. Toggling this box will either enable or disable if the Rigidbody it will be sleeping on the start of your scene.

Also within the Edit menu is two new options:
- 'Enable Sleep On Awake For All Rigidbodies In Scene'
- 'Disable Sleep On Awake For All Rigidbodies In Scene'

These will enable/disable sleeping on awake for all Rigidbodies within the current scene.

Note: As enabling 'Sleep On Awake' adds an additional component to your GameObject, if the GameObject is a prefab it will automatically prompt you confirm if you wish to break the prefab, cancel or apply it directly to the prefab itself.