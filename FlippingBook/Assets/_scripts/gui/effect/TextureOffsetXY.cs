using UnityEngine;
using System.Collections;

public class TextureOffsetXY : MonoBehaviour
{
	public float speedx = 0.5f;
    public float speedy = 0.0f;
	
	
	private float offsetx = 0;
    private float offsety = 0;

	void Update()
	{
		offsetx += speedx * Time.deltaTime;
		if(offsetx > 1)
			offsetx = 0;
        offsety += speedy * Time.deltaTime;
        if(offsety > 1)
            offsety = 0;
		
		GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(offsetx, offsety));
	}
}

