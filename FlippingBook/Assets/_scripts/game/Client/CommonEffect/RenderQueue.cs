﻿using UnityEngine;
using System.Collections;

public class RenderQueue : MonoBehaviour {

	public bool useSharedMaterial;
	public int queue = 4000;
	
	void Start () {
		if(useSharedMaterial)
			GetComponent<Renderer>().sharedMaterial.renderQueue = queue;
		else
			GetComponent<Renderer>().material.renderQueue = queue;
	}

}
